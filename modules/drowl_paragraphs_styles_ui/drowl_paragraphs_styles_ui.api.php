<?php

/**
 * Alter the list of drowl_paragraphs_styles_ui_definitions.
 *
 * Add, modify or remove definitions from the arry.
 *
 *
 * @param [type] $definitions
 */
function HOOK_drowl_paragraphs_styles_ui_definitions_alter(&$definitions) {
  // ADD example:
  $iconPathPrefix = base_path() . \Drupal::service('extension.list.module')->getPath('my_module_name');
  $definitions['boxStyleGreen'] = [
    'label' => t('Box Style: Green'),
    'classesString' => 'box-style box-style--green',
    'description' => t('Lorem ipsum dolar sit'),
    'iconPath' => $iconPathPrefix . '/images/icons/boxStyleGreen.svg',
  ];

  // MODIFY example:
  $definitions['boxStyleBlack']['classesString'] .= ' lorem-ipsum-class';

  // REMOVE example:
  unset($definitions);
}

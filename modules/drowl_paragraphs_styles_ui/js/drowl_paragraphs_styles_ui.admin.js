/**
 * @file
 * Drowl paragraphs styles ui admin JS.
 *
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.drowl_paragraphs_styles_ui_admin = {
    attach(context, settings) {
      const styleDefinitions =
        drupalSettings.drowl_paragraphs_styles_ui.definitions;
      function updateStylesUi($stylesWrapper, activeClassStrings) {
        // Receive array of active classesStringtrings
        // Search for them in the styles array
        // set/remove the active classes on the UI items
        const $styles = $stylesWrapper.querySelectorAll(
          ".drowl-paragraphs-styles__item"
        );
        for (let i = 0; i < $styles.length; i++) {
          let $style = $styles[i];
          let styleString = $style.getAttribute("data-style-selector");
          if (activeClassStrings.includes(styleString)) {
            $style.classList.add("drowl-paragraphs-styles__item--active");
          } else {
            // Empty classes
            $style.classList.remove("drowl-paragraphs-styles__item--active");
          }
        }
      }
      function getInputClassString(classFieldId) {
        const classString = document.getElementById(classFieldId).value;
        if (classString.length) {
          return classString;
        }
        return "";
      }
      function setInputClassString(classFieldId, classesString) {
        let newValue = "";
        if (classesString.length) {
          newValue = classesString;
        }
        document.getElementById(classFieldId).setAttribute("value", newValue);
      }
      function addClassesString(classesString, classStringToAdd) {
        return (classesString + " " + classStringToAdd)
          .replace(/  +/g, " ")
          .trim();
      }
      function removeClassesString(classesString, classStringToRemove) {
        return classesString
          .replace(classStringToRemove, "")
          .replace(/  +/g, " ")
          .trim();
      }

      // TODO: Better empty check? If no styleDefinitions present its an empty array,
      // if styleDefinitions present, its an object.
      if (styleDefinitions) {
        const $classFields = context.querySelectorAll(
          '[name^="field_paragraph_settings"][name$="[style][expert][classes_additional]"]'
        );
        for (let i = 0; i < $classFields.length; i++) {
          const $classField = $classFields[i];
          const classFieldId = $classField.getAttribute("id");
          const $stylesTabPanelsWrapper = $classField.closest(
            ".horizontal-tabs-panes"
          );
          const $stylesTabPanel = $stylesTabPanelsWrapper.querySelector(
            '[id*="style-style-boxstyle"] .details-wrapper'
          );
          let activeclassesString = getInputClassString(classFieldId);
          // Add Styles UI Markup
          if (
            $stylesTabPanel &&
            !$stylesTabPanel.querySelectorAll(".drowl-paragraphs-styles").length
          ) {
            $stylesTabPanel.insertAdjacentHTML(
              "afterbegin",
              `<div class='drowl-paragraphs-styles'><div class='drowl-paragraphs-styles__title'>${Drupal.t(
                "Paragraphs Styles"
              )}</div><div class='drowl-paragraphs-styles__styles'></div><button class='drowl-paragraphs-styles__reset button'>Remove all</button></div>`
            );
            const $stylesWrapper = $stylesTabPanel.querySelector(
              ".drowl-paragraphs-styles__styles"
            );
            const $stylesResetButton = $stylesTabPanel.querySelector(
              ".drowl-paragraphs-styles__reset"
            );

            // eslint-disable-next-line no-restricted-syntax
            for (const key of Object.keys(styleDefinitions)) {
              const style = styleDefinitions[key];
              let styleActiveClass = "";
              if (
                activeclassesString.length &&
                activeclassesString.includes(style.classString)
              ) {
                styleActiveClass = " drowl-paragraphs-styles__item--active";
              }
              $stylesWrapper.innerHTML += `<div data-style-selector="${style.classesString}" class='drowl-paragraphs-styles__item drowl-paragraphs-styles__item--${key}${styleActiveClass}'>
            <div class='drowl-paragraphs-styles__item-image'><img src="${style.iconPath}"></div>
            <div class='drowl-paragraphs-styles__item-name'>${style.label}</div>
            <div class='drowl-paragraphs-styles__item-desc'>${style.description}</div>
          </div>`;
            }
            const $styles = $stylesWrapper.querySelectorAll(
              ".drowl-paragraphs-styles__item"
            );
            // Initialy check for existing classstrings
            updateStylesUi($stylesWrapper, getInputClassString(classFieldId));
            // Register Styles reset button
            $stylesResetButton.addEventListener("click", function (e) {
              e.preventDefault();
              setInputClassString(classFieldId, "");
              updateStylesUi($stylesWrapper, "");
            });
            // Register UI click events
            for (let i = 0; i < $styles.length; i++) {
              $styles[i].addEventListener("click", function () {
                const $style = $styles[i];
                const styleClassString = $style.getAttribute(
                  "data-style-selector"
                );
                activeclassesString = getInputClassString(classFieldId);
                if (
                  activeclassesString.length &&
                  activeclassesString.includes(styleClassString)
                ) {
                  activeclassesString = removeClassesString(
                    activeclassesString,
                    styleClassString
                  );
                } else {
                  activeclassesString = addClassesString(
                    activeclassesString,
                    styleClassString
                  );
                }
                setInputClassString(classFieldId, activeclassesString);
                updateStylesUi($stylesWrapper, activeclassesString);
              });
            }
          }
        }
      }
    },
  };
})(jQuery, Drupal, drupalSettings);

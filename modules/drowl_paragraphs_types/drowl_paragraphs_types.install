<?php

/**
 * @file
 */

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Extension\MissingDependencyException;

/**
 * @file
 * Install, update and uninstall functions for drowl_paragraphs_types.
 */

/**
 *
 */
function drowl_paragraphs_types_update_dependencies() {
  // drowl_paragraphs_types_update_4001 must run
  // before drowl_paragraphs_update_8300 otherwise the required information
  // was already deleted.
  $dependencies['drowl_paragraphs'][8300] = [
    'drowl_paragraphs_types' => 8401,
    'drowl_paragraphs_types' => 8402,
  ];
  return $dependencies;
}

/**
 * Enable responsive_background_image module which is a dependency.
 */
function drowl_paragraphs_types_update_8400(&$sandbox) {
  try {
    if (\Drupal::service('module_handler')->moduleExists('responsive_background_image')) {
      return t('Module "@module" is already enabled. Nothing to do."', ['@module' => 'responsive_background_image']);
    }
    else {
      if (\Drupal::service('module_installer')->install(['responsive_background_image'])) {
        return 'Enabled Module "responsive_background_image" successfully.';
      }
    }
  }
  catch (MissingDependencyException $e) {
    return t('Module "@module" was not enabled due to the following error: "@message"', ['@module' => 'media_entity_actions', '@message' => $e->getMessage()]);
  }
  catch (Exception $e) {
    return $e->getMessage();
  }
}

/**
 * Convert paragraph bundle "bild_text" to bundle "image_text" in config.
 * Values are handled separately on post_update hook.
 */
function drowl_paragraphs_types_update_8401(&$sandbox) {
  // We can't change the bundle type in config, so we import
  // image_text from install config and delete bild_text later.
  $entityTypeBundleInfo = \Drupal::service('entity_type.bundle.info');
  $paragraphBundleInfoArray = $entityTypeBundleInfo->getBundleInfo('paragraph');
  if (!array_key_exists('image_text', $paragraphBundleInfoArray)) {
    // Exclude field.field.paragraph.image_text.field_paragraph_settings as
    // at this points it would have a different schema!
    _drowl_paragraphs_types_ensure_bundle_from_config('image_text', TRUE, ['field.field.paragraph.image_text.field_paragraph_settings']);
  }
  else {
    \Drupal::logger('drowl_paragraphs_types')->info('@function: Bundle image_text already exists. No need to convert legacy "bild_text".', [
      '@function' => __FUNCTION__,
    ]);
    return 'Bundle image_text already exists. No need to convert legacy "bild_text".';
  }

  $database = \Drupal::database();
  // This has to be done in database as Entities don't allow this dirty change yet...
  /*
  base_table = "paragraphs_item" ($entity_type->getBaseTable())
  data_table = "paragraphs_item_field_data" ($entity_type->getDataTable())
  revision_table = "paragraphs_item_revision" ($entity_type->getRevisionTable())
  revision_data_table = "paragraphs_item_revision_field_data" ($entity_type->getRevisionDataTable())
   */
  $paragraphEntityTypeDefinition = \Drupal::service('entity_type.manager')->getDefinition('paragraph');
  // Wrap this into a transaction:
  try {
    $transaction = $database->startTransaction();

    $entityTypeStorage = \Drupal::service('entity_type.manager')->getStorage('paragraph');
    $query = $entityTypeStorage->getQuery();
    $query->accessCheck(FALSE)->condition('type', 'bild_text');
    $results = $query->execute();
    $bildTextParagraphs = $entityTypeStorage->loadMultiple($results);
    if (!empty($bildTextParagraphs)) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      foreach ($bildTextParagraphs as $bildTextParagraph) {
        // Update the base table, set type from "bild_text" to "image_text", keep everything else!
        $database->update($paragraphEntityTypeDefinition->getBaseTable())
          // As we'll remove bild_text completely, we don't even care for the paragraph id:
          // ->condition('id', $bildTextParagraph->id())
          // Do not respect the revision id or langcode as this conversion isn't backwards compatible at all:
          // ->condition('revision_id', $bildTextParagraph->getRevisionId())
          // ->condition('langcode', $bildTextParagraph->language()->getId())
          ->condition('type', 'bild_text')
          ->fields([
            'type' => 'image_text',
          ])->execute();
        \Drupal::logger('drowl_paragraphs_types')->info('Updated all affected type colums from bild_text to image_text in "@table" ', ['@table' => $paragraphEntityTypeDefinition->getBaseTable()]);

        // Update the data table, set type from "bild_text" to "image_text", keep everything else!
        $database->update($paragraphEntityTypeDefinition->getDataTable())
          // As we'll remove bild_text completely, we don't even care for the paragraph id:
          // ->condition('id', $bildTextParagraph->id())
          // Do not respect the revision id or langcode as this conversion isn't backwards compatible at all:
          // ->condition('revision_id', $bildTextParagraph->getRevisionId())
          // ->condition('langcode', $bildTextParagraph->language()->getId())
          ->condition('type', 'bild_text')
          ->fields([
            'type' => 'image_text',
          ])->execute();
        \Drupal::logger('drowl_paragraphs_types')->info('Updated all affected type colums from bild_text to image_text in "@table" ', ['@table' => $paragraphEntityTypeDefinition->getDataTable()]);
      }
    }
    else {
      \Drupal::logger('drowl_paragraphs_container2layout')->notice('No bild_text paragraphs found to update.');
    }
    // paragraphs_item_revision and paragraphs_item_revision_field_data don't contain relevant data.
    // Now we also have to change all fields referencing the bundle:
    $paragraphBundleContainerFieldDefinitions = Drupal::service('entity_field.manager')->getFieldDefinitions('paragraph', 'bild_text');
    $table_mapping = Drupal::service('entity_type.manager')->getStorage('paragraph')->getTableMapping();
    $field_table_names = [];
    foreach ($paragraphBundleContainerFieldDefinitions as $field_key => $field) {
      if ($field->getFieldStorageDefinition()->isBaseField() == FALSE) {
        // We do not convert base fields.
        $field_name = $field->getName();
        $field_table = $table_mapping->getFieldTableName($field_name);
        $field_table_names[$field_name] = $field_table;
        $field_storage_definition = $field->getFieldStorageDefinition();
        $field_revision_table = $table_mapping->getDedicatedRevisionTableName($field_storage_definition);
        // Field revision tables DO have the bundle!
        $field_table_names[$field_name . '_revision'] = $field_revision_table;
      }
    }
    \Drupal::logger('drowl_paragraphs_types')->info('Updating tables: @tables', ['@tables' => implode(', ', $field_table_names)]);

    foreach ($field_table_names as $field_table_name) {
      \Drupal::logger('drowl_paragraphs_types')->info('Updating table: @table', ['@table' => $field_table_name]);
      if ($database->schema()->tableExists($field_table_name)) {
        $database->update($field_table_name)
          // We do this globally once for the table, the ID is not relevant for us here:
          // ->condition('entity_id', $bildTextParagraph->id())
          // Do not respect the revision id or langcode as this conversion isn't backwards compatible at all:
          // ->condition('revision_id', $containerParagraph->getRevisionId())
          // ->condition('langcode', $containerParagraph->language()->getId())
          ->condition('bundle', 'bild_text')
          ->fields([
            'bundle' => 'image_text',
          ])->execute();
        \Drupal::logger('drowl_paragraphs_types')->info('Updated all affected bundle colums from bild_text to image_text in "@table" ', ['@table' => $field_table_name]);
      }
      else {
        \Drupal::logger('drowl_paragraphs_container2layout')->notice('@function: Field table @table does not exist. Skipping for update.', [
          '@function' => __FUNCTION__,
          '@table' => $field_table_name,
        ]);
      }
    }
  }
  catch (Exception $e) {
    // Something went wrong somewhere, so roll back now.
    $transaction
      ->rollBack();

    // Rethrow exception:
    throw $e;
  }

  // Delete paragraph bild_text bundle!
  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $entity_storage */
  $entity_storage = \Drupal::entityTypeManager()->getStorage('paragraphs_type');
  $configEntityBildText = $entity_storage->load('bild_text');
  if (!empty($configEntityBildText)) {
    $configEntityBildText->delete();
    \Drupal::logger('drowl_paragraphs_types')->info('Deleted paragraphs bundle bild_text.');
    return 'Deleted paragraphs bundle bild_text.';
  }

}

/**
 * Move values from former drowl_paragraphs_setting field value
 * field_paragraph_settings_layout_reverse_order into
 * field_paragraphs_media_alignment for paragraph bunldes
 * - image_text
 * - video_text
 * to keep their media / text alignment.
 *
 * @see https://www.drupal.org/node/3260848
 */
function drowl_paragraphs_types_update_8402(&$sandbox) {
  // This was added later for sites which didn't upgrade yet
  // So we can still use the value from 'layout_reverse_order' for
  // image_text & video_text paragraphs migrated into the new field
  // field_paragraphs_media_alignment:
  $image_text_messages = _drowl_paragraphs_types_ensure_field_from_config('field_paragraphs_media_alignment', 'image_text', 'paragraph');
  $video_text_messages = _drowl_paragraphs_types_ensure_field_from_config('field_paragraphs_media_alignment', 'video_text', 'paragraph');

  return implode("\n", array_merge($image_text_messages, $video_text_messages));
}

/**
 * Add a new allowed value.
 *
 * Add a new value "tabs_only|Tabs (on all devices)" to
 * "field.field.paragraph.container_tabs_accordion.field_paragraphs_tabs_acc_type.yml".
 */
function drowl_paragraphs_types_update_8403(&$sandbox) {
  $config = Drupal::entityTypeManager()->getStorage('field_storage_config')->load('paragraph.field_paragraphs_tabs_acc_type');
  $allowed_values = $config->getSetting('allowed_values');
  // Add new value "tabs_only|Tabs (on all devices)":
  $allowed_values['tabs_only'] = 'Tabs (on all devices)';
  $config->setSetting('allowed_values', $allowed_values)->save();
}

/**
 * Add a new allowed value for paragraph.field_paragraphs_icon_options.
 *
 * Add a new value "ico__hollow|Hollow Style" to
 * "field.storage.paragraph.field_paragraphs_icon_options.yml".
 */
function drowl_paragraphs_types_update_8404(&$sandbox) {
  $config = Drupal::entityTypeManager()->getStorage('field_storage_config')->load('paragraph.field_paragraphs_icon_options');
  $allowed_values = $config->getSetting('allowed_values');
  // Add new value "ico__hollow|Hollow Style":
  $allowed_values['ico__hollow'] = 'Hollow Style';
  $config->setSetting('allowed_values', $allowed_values)->save();
}

/**
 * Add a new allowed value.
 *
 * Add a new exclude setting "media_object:media_object" to
 * "field.field.paragraph.attachments.field_paragraphs_nodeentityrefvm.yml".
 */
function drowl_paragraphs_types_update_8405(&$sandbox) {
  $config = Drupal::entityTypeManager()->getStorage('field_config')->load('paragraph.attachments.field_paragraphs_nodeentityrefvm');
  $exclude_values = $config->getSetting('exclude');
  // Add new value media_object:media_object:
  $exclude_values['media_object'] = 'media_object';
  $config->setSetting('exclude', $exclude_values)->save();
}

/**
 * Add a new allowed value: clear for field_paragraphs_button_options.
 */
function drowl_paragraphs_types_update_8406(&$sandbox) {
  $config = Drupal::entityTypeManager()->getStorage('field_storage_config')->load('paragraph.field_paragraphs_button_options');
  $allowed_values = $config->getSetting('allowed_values');
  // Add new value "clear|Transparent, no border":
  $allowed_values['clear'] = 'Transparent, no border';
  $config->setSetting('allowed_values', $allowed_values)->save();
}

/**
 * Converts the values from the field_paragraph_settings field value
 * "layout_reverse_order" into the "field_paragraphs_media_alignment" paragraph
 * entity field of
 * - image_text
 * - video_text
 * fields.
 */
function drowl_paragraphs_types_post_update_8402_convert_media_alignment_to_field_values(&$sandbox) {
  $entityTypeStorage = Drupal::service('entity_type.manager')->getStorage('paragraph');
  $query = $entityTypeStorage->getQuery();
  $query->accessCheck(FALSE)->condition('type', ['image_text', 'video_text'], 'IN');
  $results = $query->execute();
  $paragraphs = $entityTypeStorage->loadMultiple($results);
  if (!empty($paragraphs)) {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    foreach ($paragraphs as $paragraph) {
      if (!$paragraph->hasField('field_paragraph_settings')) {
        \Drupal::logger('drowl_paragraphs_types')->notice('@function: No field_paragraph_settings (source) on @bundle Paragraph with ID: @id', [
          '@function' => __FUNCTION__,
          '@bundle' => $paragraph->bundle(),
          '@id' => $paragraph->id(),
        ]);
        // Skip this paragraph.
        continue;
      }
      if (!$paragraph->hasField('field_paragraphs_media_alignment')) {
        \Drupal::logger('drowl_paragraphs_types')->notice('@function: No field_paragraphs_media_alignment (target) on @bundle Paragraph with ID: @id', [
          '@function' => __FUNCTION__,
          '@bundle' => $paragraph->bundle(),
          '@id' => $paragraph->id(),
        ]);
        // Skip this paragraph.
        continue;
      }
      $paragraphSettingsArray = $paragraph->get('field_paragraph_settings')->getValue();
      if (!empty($paragraphSettingsArray[0]) && isset($paragraphSettingsArray[0]['layout_reverse_order'])) {
        if (!empty($paragraphSettingsArray[0]['layout_reverse_order'])) {
          // Medium right.
          $value = 'col_right';
        }
        else {
          // Medium left.
          $value = 'col_left';
        }
        $paragraph->set('field_paragraphs_media_alignment', $value);
        $paragraph->save();
        \Drupal::logger('drowl_paragraphs_types')->info('Set field_paragraphs_media_alignment (target) to "@value" determined from field_paragraph_settings (source) values on @bundle Paragraph with ID: @id', [
          '@function' => __FUNCTION__,
          '@value' => $value,
          '@bundle' => $paragraph->bundle(),
          '@id' => $paragraph->id(),
        ]);
      }
      else {
        \Drupal::logger('drowl_paragraphs_types')->notice('@function: No field_paragraph_settings (source) values on @bundle Paragraph with ID: @id', [
          '@function' => __FUNCTION__,
          '@bundle' => $paragraph->bundle(),
          '@id' => $paragraph->id(),
        ]);
      }
    }
  }
  else {
    return 'No image_text or video_text paragraphs to migrate layout_reverse_order into field_paragraphs_media_alignment.';
  }
}

/**
 * Helper function to import a bundle from config which might not yet exist
 * in active configuration.
 *
 * @param string $bundle_id
 * @param string $entity_type_id
 * @param bool $ensureFields
 *
 * @return bool
 */
function _drowl_paragraphs_types_ensure_bundle_from_config(string $bundle_id, $ensureFields = TRUE, array $exclude = []) {
  $bundle_config_name = 'paragraphs.paragraphs_type.' . $bundle_id;
  _drowl_paragraphs_types_update_from_install_config($bundle_config_name);

  if ($ensureFields) {
    $bundleFieldConfigNamePrefix = 'field.field.paragraph.' . $bundle_id . '.';
    $installConfig = new FileStorage(__DIR__ . '/config/install');
    // Get all fields from this bundle from config:
    $bundleFieldsConfigNames = $installConfig->listAll($bundleFieldConfigNamePrefix);
    if (!empty($bundleFieldsConfigNames)) {
      foreach ($bundleFieldsConfigNames as $bundleFieldConfigName) {
        if (in_array($bundleFieldConfigName, $exclude)) {
          // Skip excluded:
          continue;
        }
        $bundleFieldConfig = $installConfig->read($bundleFieldConfigName);
        _drowl_paragraphs_types_ensure_field_from_config($bundleFieldConfig['field_name'], $bundle_id, 'paragraph');
      }
    }
  }
}

/**
 * Helper function to import a field from config which might not yet exist
 * in active configuration.
 *
 * @param string $field_name
 * @param string $bundle_id
 * @param string $entity_type_id
 *
 * @return array
 *   Messages about results.
 */
function _drowl_paragraphs_types_ensure_field_from_config(string $field_name, string $bundle_id, string $entity_type_id = 'paragraph') {
  $entityTypeManager = \Drupal::service('entity_type.manager');
  $installConfig = new FileStorage(__DIR__ . '/config/install');
  $messages = [];

  // Import the field configurations:
  $configNames = [
    'field.storage.' . $entity_type_id . '.' . $field_name,
    'field.field.' . $entity_type_id . '.' . $bundle_id . '.' . $field_name,
  ];
  foreach ($configNames as $config_name) {
    _drowl_paragraphs_types_update_from_install_config($config_name);
  }

  // Add new fields to entity_view_display:
  if ($view_displays = $entityTypeManager->getStorage('entity_view_display')->loadByProperties([
    'targetEntityType' => $entity_type_id,
    'bundle' => $bundle_id,
  ])) {
    foreach ($view_displays as $view_display) {
      if ($view_display->getComponent($field_name) === NULL) {
        // Component doesn't yet exist, also import the definition:
        $entity_view_display_config_name = 'core.entity_view_display.' . $entity_type_id . '.' . $bundle_id . '.' . $view_display->getMode();
        $entity_view_display_config_array = $installConfig->read($entity_view_display_config_name);
        if (!empty($entity_view_display_config_array['content'][$field_name])) {
          $view_display->setComponent($field_name, $entity_view_display_config_array);
          $view_display->save();
          $message = t('Updated "@entity_type_id" view display from install config.', ['@entity_type_id' => $entity_type_id]);
          \Drupal::logger('drowl_paragraphs_types')->info($message);
          $messages[] = $message;
        }
      }
    }
  }

  // Add new fields to entity_form_display:
  if ($form_displays = \Drupal::service('entity_type.manager')->getStorage('entity_form_display')->loadByProperties([
    'targetEntityType' => $entity_type_id,
    'bundle' => $bundle_id,
  ])) {
    foreach ($form_displays as $form_display) {
      if ($form_display->getComponent($field_name) === NULL) {
        // Component doesn't yet exist, also import the definition:
        $entity_form_display_config_name = 'core.entity_form_display.' . $entity_type_id . '.' . $bundle_id . '.' . $form_display->getMode();
        $entity_form_display_config_array = $installConfig->read($entity_form_display_config_name);
        if (!empty($entity_form_display_config_array['content'][$field_name])) {
          $form_display->setComponent($field_name, $entity_form_display_config_array);
          $form_display->save();
          $message = t('Updated "@entity_type_id" form display from install config.', ['@entity_type_id' => $entity_type_id]);
          $message = t('Updated "@entity_type_id" view display from install config.', ['@entity_type_id' => $entity_type_id]);
          \Drupal::logger('drowl_paragraphs_types')->info($message);
          $messages[] = $message;
        }
      }
    }
  }
  return $messages;
}

/**
 * Installs (if not yet existing) or updates the active config with values
 * from install config.
 *
 * @param string $config_name
 *   The configuration name.
 */
function _drowl_paragraphs_types_update_from_install_config(string $config_name): bool {
  $installConfigDir = __DIR__ . '/config/install';
  $installConfig = new FileStorage($installConfigDir);
  $config_record = $installConfig->read($config_name);

  /** @var \Drupal\Core\Config\ConfigManagerInterface $config_manager */
  $config_manager = \Drupal::service('config.manager');
  $type = $config_manager->getEntityTypeIdByName($config_name);
  $entityTypeManager = \Drupal::service('entity_type.manager');
  $definition = $entityTypeManager->getDefinition($type);
  $id_key = $definition->getKey('id');
  $id = $config_record[$id_key];

  if (empty($config_record)) {
    // The given config wasn't found in $installConfig.
    // Check if it already exists in active configuration, otherwise
    // throw an error aw we need it.
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $activeConfigFactory */
    $activeConfigFactory = \Drupal::service('config.factory');
    $existsInActiveConfig = !$activeConfigFactory->get($config_name)->isNew();
    if (!$existsInActiveConfig) {
      throw new Exception('Configuration was neither found in "' . $installConfigDir . '" nor in active configuration. Aborting here, as we need it.');
    }

    \Drupal::logger('drowl_paragraphs_types')->warning('Configuration "@config_name" was not updated from "@installConfigDir" as it could not be found there, but already exists in active configuration.', ['@installConfigDir' => $installConfigDir, '@config_name' => $config_name, '@installConfigDir' => $installConfigDir]);
    // Otherwise return as the config already exists in active configuration
    // and keep as-is.
    return FALSE;
  }
  // \Drupal::logger('drowl_paragraphs_types')->notice($config_name . ' | Type: ' . $type . ' | ID-Key: ' . $id_key . ' | ID: ' . $id);

  /** @var \Drupal\Core\Config\Entity\ConfigEntityStorage $entity_storage */
  $entity_storage = \Drupal::entityTypeManager()->getStorage($type);
  $configEntity = $entity_storage->load($id);
  if ($configEntity) {
    $configEntity = $entity_storage->updateFromStorageRecord($configEntity, $config_record);
    $configEntity->save();
    \Drupal::logger('drowl_paragraphs_types')->info('Configuration "@config_name" was updated from "@installConfigDir" successfully. Active configuration has been overwritten.', ['@config_name' => $config_name, '@installConfigDir' => $installConfigDir]);
  }
  else {
    $configEntity = $entity_storage->createFromStorageRecord($config_record);
    $configEntity->save();
    \Drupal::logger('drowl_paragraphs_types')->info('Configuration "@config_name" was created from "@installConfigDir" successfully. Did not exist in active configuration before.', ['@config_name' => $config_name, '@installConfigDir' => $installConfigDir]);
  }
  return TRUE;
}

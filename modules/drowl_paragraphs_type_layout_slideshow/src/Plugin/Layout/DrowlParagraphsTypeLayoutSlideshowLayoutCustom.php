<?php

namespace Drupal\drowl_paragraphs_type_layout_slideshow\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Fixed / equal column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlParagraphsTypeLayoutSlideshowLayoutCustom extends DrowlParagraphsTypeLayoutSlideshowLayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'visible_elements_sm' => NULL,
      'visible_elements_md' => NULL,
      'visible_elements_lg' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();

    $visibleElementsOptions = array_combine(range(1, 12), range(1, 12));

    // -- visible_elements_sm --
    $field_name = 'visible_elements_sm';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_paragraphs_type_layout_slideshow_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (small devices)'),
      '#options' => $visibleElementsOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Sets the number of visible elements for small and larger devices.'),
      '#wrapper_attributes' => ['class' => ['form-item--visible-elements-sm']],
    ];

    // -- visible_elements_md --
    $field_name = 'visible_elements_md';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_paragraphs_type_layout_slideshow_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (medium devices)'),
      '#options' => $visibleElementsOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Sets the number of visible elements for medium and larger devices.'),
      '#wrapper_attributes' => ['class' => ['form-item--visible-elements-md']],
    ];

    // -- visible_elements_lg --
    $field_name = 'visible_elements_lg';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_paragraphs_type_layout_slideshow_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (large devices)'),
      '#options' => $visibleElementsOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Sets the number of visible elements for large devices.'),
      '#wrapper_attributes' => ['class' => ['form-item--visible-elements-lg']],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['visible_elements_sm'] = $form_state->getValue('visible_elements_sm_wrapper')['visible_elements_sm'];
    $this->configuration['visible_elements_md'] = $form_state->getValue('visible_elements_md_wrapper')['visible_elements_md'];
    $this->configuration['visible_elements_lg'] = $form_state->getValue('visible_elements_lg_wrapper')['visible_elements_lg'];
  }
}


<?php

namespace Drupal\drowl_paragraphs_type_layout_slideshow\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Layout plugin class to provide general options for Layout Slideshow Layouts.
 *
 * @internal
 *   Plugin classes are internal.
 */
trait DrowlParagraphsTypeLayoutSlideshowSettingsLayoutTrait {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      // Set OUR defaults:
      'layout_section_width' => NULL,
      'autoplay' => NULL,
      'auto_height' => NULL,
      'navigation_arrows' => NULL,
      'navigation_dots' => NULL,
      'infinite' => NULL,
      'center_mode' => NULL,
      'controls_outside' => NULL,
      'extra_classes' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    $booleanOptions = [
      NULL => $this->t('- Global default -'),
      TRUE => $this->t('Yes'),
      FALSE => $this->t('No'),
    ];

    // Section width:
    $field_name = 'layout_section_width';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Section width'),
      '#options' => [
        'page-width' => $this->t('Page width (all)'),
        'viewport-width' => $this->t('Viewport width (all)'),
        'viewport-width-cp' => $this->t('Viewport width (only background)'),
      ],
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Overrides the container width, ignoring the parent container width. Viewport width = screen width, Page width = content width.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-section-width']],
    ];

    // Show arrows:
    $field_name = 'autoplay';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Autoplay'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Plays the slideshow automatically. If this option is deactivated, the user has to forward manually.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-autoplay']],
    ];

    // Automatic height:
    $field_name = 'auto_height';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Automatic height'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Automatically detects each slides height and accordingly changes the slideshow height on scroll.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-auto-height']],
    ];

    // Show arrows:
    $field_name = 'navigation_arrows';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Show arrows navigation'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Shows back / forward navigation arrows.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-navigation-arrows']],
    ];

    // Show dots:
    $field_name = 'navigation_dots';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Show dots navigation'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Show navigation dots.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-navigation-dots']],
    ];

    // Infinite:
    $field_name = 'infinite';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Infinite loop'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Displays the slideshow infinitely by restarting at the first element after the last.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-infinite']],
    ];

    // Center mode:
    $field_name = 'center_mode';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Center mode'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      // @todo: Get global default value here and show at the end of the description (translation variable):
      '#description' => $this->t('Allows a centered view by presenting the previous and next slide cropped. For center mode to work, the number of visible slides must be set to an odd number.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-center-mode']],
    ];

    // Controls outside:
    $field_name = 'controls_outside';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_' . $field_name,
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Show controls outside'),
      '#options' => $booleanOptions,
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- Global default -'),
      '#default_value' => $configuration[$field_name],
      '#required' => FALSE,
      '#description' => $this->t('Shows the controls (arrows / dots) outside the slider so as not to obscure the content.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-controls-outside']],
    ];

    // Extra classes:
    $form['extra_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Extra classes'),
      '#default_value' => $configuration['extra_classes'],
      '#weight' => 999,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo We should better massage the form values here, but neither #tree = true nor other things work... So we use the nested array so far.
    $this->configuration['layout_section_width'] = $form_state->getValue('layout_section_width_wrapper')['layout_section_width'];
    $this->configuration['autoplay'] = $form_state->getValue('autoplay_wrapper')['autoplay'];
    $this->configuration['auto_height'] = $form_state->getValue('auto_height_wrapper')['auto_height'];
    $this->configuration['navigation_arrows'] = $form_state->getValue('navigation_arrows_wrapper')['navigation_arrows'];
    $this->configuration['navigation_dots'] = $form_state->getValue('navigation_dots_wrapper')['navigation_dots'];
    $this->configuration['infinite'] = $form_state->getValue('infinite_wrapper')['infinite'];
    $this->configuration['center_mode'] = $form_state->getValue('center_mode_wrapper')['center_mode'];
    $this->configuration['controls_outside'] = $form_state->getValue('controls_outside_wrapper')['controls_outside'];
    $this->configuration['extra_classes'] = $form_state->getValue('extra_classes');
  }

}

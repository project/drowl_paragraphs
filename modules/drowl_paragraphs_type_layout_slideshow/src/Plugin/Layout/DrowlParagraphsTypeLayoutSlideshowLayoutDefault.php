<?php

namespace Drupal\drowl_paragraphs_type_layout_slideshow\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Layout\LayoutDefault;

/**
 * Fixed / equal column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlParagraphsTypeLayoutSlideshowLayoutDefault extends LayoutDefault implements PluginFormInterface {
  use DrowlParagraphsTypeLayoutSlideshowSettingsLayoutTrait;

}


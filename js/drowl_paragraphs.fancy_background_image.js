/**
 * @file
 * DROWL Paragraphs frontend JavaScript
 *
 */
(function ($, Drupal) {
  Drupal.behaviors.drowl_paragraphs_fancy_background_image = {
    attach: function (context, settings) {
      $(once("fancy-background-image", ".fancy-background-image--parallax", context))
        .each(function () {
          var $sectionWrapper = $(this);
          var $backgroundContainer = $sectionWrapper.find(
            ".fancy-background-image__img-wrapper:first"
          );
          // Also can pass in optional settings block
          var rellax = new Rellax($backgroundContainer[0], {
            // wrapper: $sectionWrapper[0],
            vertical: true,
            horizontal: false,
            speed: -5,
            center: true,
          });
        });
    },
  };
})(jQuery, Drupal);

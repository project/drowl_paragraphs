<?php

namespace Drupal\drowl_paragraphs\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Field widget "drowl_paragraphs_settings_default".
 *
 * @FieldWidget(
 *   id = "drowl_paragraphs_settings_default",
 *   label = @Translation("DROWL Paragraphs settings default"),
 *   field_types = {
 *     "drowl_paragraphs_settings",
 *   }
 * )
 */
class DrowlParagraphsSettingsDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, ConfigFactoryInterface $config_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'open' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display element open by default.'),
      '#default_value' => $this->getSetting('open'),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('open')) {
      $summary[] = $this->t('Display element open by default.');
    }
    else {
      $summary[] = $this->t('Display element closed by default.');
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Get global config settings:
    $configSettings = $this->configFactory->get('drowl_paragraphs.settings');

    // $item is where the current saved values are stored.
    $item = &$items[$delta];

    // Attach backend library:
    $form['#attached']['library'][] = 'drowl_paragraphs/drowl-paragraphs-backend';

    // Common options:
    $percentage_options = [
      '10' => '10%',
      '20' => '20%',
      '30' => '30%',
      '40' => '40%',
      '50' => '50%',
      '60' => '60%',
      '70' => '70%',
      '80' => '80%',
      '90' => '90%',
      '100' => '100%',
    ];

    $distance_options = [
    // Used for reset / override purposes.
      'none' => '0',
      'xxs' => 'xxs',
      'xs' => 'xs',
      'sm' => 's',
      'md' => 'm',
      'lg' => 'l',
      'xl' => 'xl',
      'xxl' => 'xxl',
    ];

    // ===================================
    // Styling group
    // ===================================
    $element['style'] = [
      '#type' => 'horizontal_tabs',
      '#theme_wrappers' => ['horizontal_tabs'],
      '#title' => $this->t('Style'),
      '#tree' => TRUE,
    ];

    $element['style']['spacing'] = [
      '#type' => 'details',
      '#title' => $this->t('Spacings'),
      '#group' => 'style',
      '#group_name' => 'style',
      '#description' => '<i class="fa fa-info-circle" aria-hidden="true"></i> <strong>' . $this->t('Help') . ':</strong> ' . $this->t('Custom spacings - use with care, this could possibly harm the default styling.'),
    ];
    // Add further wrapper for the margin (& padding) fields (just for style purposes)
    $element['style']['spacing']['margin_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['margin-wrapper']],
      // Add Markup for the spacings preview.
      '#suffix' => '<div class="spacings-preview">
                      <strong class="spacings-preview__label">' . $this->t('Spacings Preview (horizontal & vertical)') . ':</strong>
                      <div class="spacings-preview__spacing pl-xxs"><span>XXS</span></div>
                      <div class="spacings-preview__spacing pl-xs"><span>XS</span></div>
                      <div class="spacings-preview__spacing pl-sm"><span>S</span></div>
                      <div class="spacings-preview__spacing pl-md"><span>M</span></div>
                      <div class="spacings-preview__spacing pl-lg"><span>L</span></div>
                      <div class="spacings-preview__spacing pl-xl"><span>XL</span></div>
                      <div class="spacings-preview__spacing pl-xxl mb-none"><span>XXL</span></div>
                    </div>',
    ];
    $element['style']['spacing']['margin_wrapper']['layout_margin_top'] = [
      '#type' => 'select',
      '#title' => $this->t('Margin top'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_margin_top ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-margin-top'],
      '#empty_option' => '-',
    ];
    $element['style']['spacing']['margin_wrapper']['layout_margin_right'] = [
      '#type' => 'select',
      '#title' => $this->t('Margin right'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_margin_right ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-margin-right'],
      '#empty_option' => '-',
    ];
    $element['style']['spacing']['margin_wrapper']['layout_margin_bottom'] = [
      '#type' => 'select',
      '#title' => $this->t('Margin bottom'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_margin_bottom ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-margin-bottom'],
      '#empty_option' => '-',
    ];
    $element['style']['spacing']['margin_wrapper']['layout_margin_left'] = [
      '#type' => 'select',
      '#title' => $this->t('Margin left'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_margin_left ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-margin-left'],
      '#empty_option' => '-',
    ];
    // Add further inner wrapper for the padding fields (just for style purposes)
    $element['style']['spacing']['margin_wrapper']['padding_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['padding-wrapper']],
      // Add some further elements / markup.
      '#prefix' => '<strong class="margin-label">' . $this->t('Outer') . '</strong><strong class="padding-label">' . $this->t('Inner') . '</strong>',
    ];
    $element['style']['spacing']['margin_wrapper']['padding_wrapper']['layout_padding_top'] = [
      '#type' => 'select',
      '#title' => $this->t('Padding top'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_padding_top ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-padding-top'],
      '#empty_option' => '-',
      '#prefix' => '<i class="content-placeholder fa fa-align-left fa-3x" aria-hidden="true"></i>',
    ];
    $element['style']['spacing']['margin_wrapper']['padding_wrapper']['layout_padding_right'] = [
      '#type' => 'select',
      '#title' => $this->t('Padding right'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_padding_right ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-padding-right'],
      '#empty_option' => '-',
    ];
    $element['style']['spacing']['margin_wrapper']['padding_wrapper']['layout_padding_bottom'] = [
      '#type' => 'select',
      '#title' => $this->t('Padding bottom'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_padding_bottom ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-padding-bottom'],
      '#empty_option' => '-',
    ];
    $element['style']['spacing']['margin_wrapper']['padding_wrapper']['layout_padding_left'] = [
      '#type' => 'select',
      '#title' => $this->t('Padding left'),
      '#options' => $distance_options,
      '#default_value' => $item->layout_padding_left ?? '',
      '#wrapper_attributes' => ['class' => 'form-item--layout-padding-left'],
      '#empty_option' => '-',
    ];
    $element['style']['spacing']['misc']['layout_min_height'] = [
      '#type' => 'select',
      '#title' => $this->t('Minimum height'),
      '#options' => $percentage_options,
      '#default_value' => $item->layout_min_height ?: 0,
      '#empty_option' => $this->t('- None -'),
      '#required' => FALSE,
      '#description' => $this->t('The minimum height of the element. Mostly used to set the rows items height equally.'),
      '#wrapper_attributes' => ['class' => 'form-item--layout-min-height'],
      '#empty_value' => 0,
    ];
    $element['style']['spacing']['misc']['equal_height_group'] = [
      '#title' => t('Equal height group'),
      '#type' => 'textfield',
      '#default_value' => $item->equal_height_group ?? '',
      '#description' => $this->t('Enter the name (unique identifier) of the group of items which should have an equal height.'),
      '#wrapper_attributes' => ['class' => 'form-item--layout-equal_height_group'],
    ];

    $element['style']['style_boxstyle'] = [
      '#type' => 'details',
      '#title' => $this->t('Box Style'),
      '#group' => 'style',
      '#group_name' => 'style',
    ];

    $options = $configSettings->get('options');
    $settings_style_boxstyle_options = !empty($options['options_style_boxstyle']) ? $options['options_style_boxstyle'] : [];
    $style_boxstyle_options = [
      // Add none option ('#type' => "radios" FAPI doesn't provide that).
      // See: https://julian.pustkuchen.com/beware-drupals-radios-fapi-empty-handling
      '' => $this->t('- None -'),
    ];
    foreach ($settings_style_boxstyle_options as $key => $settings_style_boxstyle_option) {
      $style_boxstyle_options[Html::CleanCssIdentifier($key)] = $this->t(Html::escape($settings_style_boxstyle_option));
    }
    $element['style']['style_boxstyle']['style_boxstyle'] = [
      '#type' => 'radios',
      '#title' => $this->t('Box style'),
      '#options' => $style_boxstyle_options,
      '#required' => FALSE,
      '#default_value' => $item->style_boxstyle ?? '',
      '#description' => $this->t('Predefined styling of this container.'),
      '#wrapper_attributes' => ['class' => 'form-item--style-boxstyle'],
      '#after_build' => ['drowl_paragraphs_process_radios_form_field_style_boxstyle'],
    ];
    $element['style']['style_boxstyle']['style_boxstyle_outline'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Box style: Only outline style'),
      '#default_value' => !empty($item->style_boxstyle_outline) ? 1 : 0,
      '#description' => $this->t('Apply the box style only to the border. Leave the background and content colors regular.'),
      '#wrapper_attributes' => ['class' => 'form-item--style-boxstyle-outline'],
      // @todo We'd need a selector to work on ancestor elements only:
      // '#states' => [
      //   'visible' => [
      //     ':input[name$="[style][style_boxstyle][style_boxstyle]"]' => array('filled' => TRUE),
      //   ],
      //   'unchecked' => [
      //     ':input[name$="[style][style_boxstyle][style_boxstyle]"]' => array('filled' => FALSE),
      //   ],
      // ],.
    ];
    $element['style']['style_boxstyle']['style_cutline'] = [
      '#type' => 'select',
      '#title' => $this->t('Cutline'),
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $item->style_cutline ?? '',
      '#description' => $this->t('Add a cutline on top or bottom of this Paragraph.'),
      '#wrapper_attributes' => ['class' => 'form-item--style-cutline'],
    ];
    $element['style']['animations'] = [
      '#type' => 'details',
      '#title' => $this->t('Animations'),
      '#group' => 'style',
      '#group_name' => 'style',
      '#description' => '<i class="fa fa-info-circle" aria-hidden="true"></i> <strong>' . $this->t('Help') . ':</strong> ' . $this->t('Set animations for this container, based on specific events.'),
    ];
    $animations_allowed_count = 4;
    for ($i = 1; $i <= $animations_allowed_count; $i++) {
      $element['style']['animations']['style_animation_' . $i] = [
        '#type' => 'details',
        '#title' => $this->t('Animation') . ' ' . $i,
      ];
      $element['style']['animations']['style_animation_' . $i]['style_animation_' . $i . '_events'] = [
        '#type' => 'select',
        '#title' => $this->t('Event trigger'),
        '#options' => [
          'enter-viewport' => $this->t('Entering the viewport'),
          'leave-viewport' => $this->t('Leaving the viewport'),
          'hover' => $this->t('Hover the element.'),
        ],
        '#required' => FALSE,
        '#multiple' => FALSE,
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $item->{'style_animation_' . $i . '_events'} ?? '',
        '#description' => '<ul>
          <li><strong>' . $this->t('Entering the viewport') . ':</strong> ' . $this->t('Animates if the element enters the viewport (by scrolling).') . '</li>
          <li><strong>' . $this->t('Leaving the viewport') . ':</strong> ' . $this->t('Animates if the element leaves the viewport (by scrolling).') . '</li>
          <li><strong>' . $this->t('Hover the element') . ':</strong> ' . $this->t('Animation starts when entering the element with the cursor.') . '</li>
        </ul>',
        '#wrapper_attributes' => ['class' => 'form-item--style-animation-events'],
      ];

      $element['style']['animations']['style_animation_' . $i]['style_animation_' . $i . '_offset'] = [
        '#type' => 'select',
        '#title' => $this->t('Viewport animation event trigger offset'),
        '#options' => [
          '0' => '0',
          '10' => $this->t('@percent% visible', ['@percent' => '10']),
          '20' => $this->t('@percent% visible', ['@percent' => '20']),
          '30' => $this->t('@percent% visible', ['@percent' => '30']),
          '40' => $this->t('@percent% visible', ['@percent' => '40']),
          '50' => $this->t('@percent% visible', ['@percent' => '50']),
          '60' => $this->t('@percent% visible', ['@percent' => '60']),
          '70' => $this->t('@percent% visible', ['@percent' => '70']),
          '80' => $this->t('@percent% visible', ['@percent' => '80']),
          '90' => $this->t('@percent% visible', ['@percent' => '90']),
          '100' => $this->t('@percent% visible', ['@percent' => '100']),
        ],
        '#default_value' => $item->{'style_animation_' . $i . '_offset'} ?: 0,
        '#empty_option' => $this->t('- None -'),
        '#empty_value' => 0,
        '#required' => FALSE,
        '#description' => $this->t('Entering the viewport') . '/' . $this->t('Leaving the viewport') . ': ' . $this->t('Offset for the animation to start if the element is visible for x %.'),
        // @todo We'd need a selector to work on ancestor elements only:
        //   '#states' => [
        //          'visible' => [
        //            'select[name$="[style][animations][style_animation_' . $i . '][style_animation_' . $i . '_events]"]:first' => [
        //              'value' => [
        //                'enter-viewport',
        //                'leave-viewport',
        //              ],
        //            ],
        //          ],
        //        ],.
        '#wrapper_attributes' => ['class' => 'form-item--style-animation-offset'],
      ];

      $element['style']['animations']['style_animation_' . $i]['style_animation_' . $i . '_animation'] = [
        '#type' => 'select',
        '#title' => $this->t('Animation'),
        '#options' => [
          $this->t('Attention Seekers')->__toString() => [
            'bounce' => $this->t('bounce'),
            'flash' => $this->t('flash'),
            'pulse' => $this->t('pulse'),
            'rubberBand' => $this->t('rubberBand'),
            'shake' => $this->t('shake'),
            'swing' => $this->t('swing'),
            'tada' => $this->t('tada'),
            'wobble' => $this->t('wobble'),
            'jello' => $this->t('jello'),
          ],
          $this->t('Bouncing Entrances')->__toString() => [
            'bounceIn' => $this->t('bounceIn'),
            'bounceInDown' => $this->t('bounceInDown'),
            'bounceInLeft' => $this->t('bounceInLeft'),
            'bounceInRight' => $this->t('bounceInRight'),
            'bounceInUp' => $this->t('bounceInUp'),
          ],
          $this->t('Bouncing Exits')->__toString() => [
            'bounceOut' => $this->t('bounceOut'),
            'bounceOutDown' => $this->t('bounceOutDown'),
            'bounceOutLeft' => $this->t('bounceOutLeft'),
            'bounceOutRight' => $this->t('bounceOutRight'),
            'bounceOutUp' => $this->t('bounceOutUp'),
          ],
          $this->t('Fading Entrances')->__toString() => [
            'fadeIn' => $this->t('fadeIn'),
            'fadeInDown' => $this->t('fadeInDown'),
            'fadeInDownBig' => $this->t('fadeInDownBig'),
            'fadeInLeft' => $this->t('fadeInLeft'),
            'fadeInLeftBig' => $this->t('fadeInLeftBig'),
            'fadeInRight' => $this->t('fadeInRight'),
            'fadeInRightBig' => $this->t('fadeInRightBig'),
            'fadeInUp' => $this->t('fadeInUp'),
            'fadeInUpBig' => $this->t('fadeInUpBig'),
          ],
          $this->t('Fading Exits')->__toString() => [
            'fadeOut' => $this->t('fadeOut'),
            'fadeOutDown' => $this->t('fadeOutDown'),
            'fadeOutDownBig' => $this->t('fadeOutDownBig'),
            'fadeOutLeft' => $this->t('fadeOutLeft'),
            'fadeOutLeftBig' => $this->t('fadeOutLeftBig'),
            'fadeOutRight' => $this->t('fadeOutRight'),
            'fadeOutRightBig' => $this->t('fadeOutRightBig'),
            'fadeOutUp' => $this->t('fadeOutUp'),
            'fadeOutUpBig' => $this->t('fadeOutUpBig'),
          ],
          $this->t('Flippers')->__toString() => [
            'flip' => $this->t('flip'),
            'flipInX' => $this->t('flipInX'),
            'flipInY' => $this->t('flipInY'),
            'flipOutX' => $this->t('flipOutX'),
            'flipOutY' => $this->t('flipOutY'),
          ],
          $this->t('Lightspeed')->__toString() => [
            'lightSpeedIn' => $this->t('lightSpeedIn'),
            'lightSpeedOut' => $this->t('lightSpeedOut'),
          ],
          $this->t('Rotating Entrances')->__toString() => [
            'rotateIn' => $this->t('rotateIn'),
            'rotateInDownLeft' => $this->t('rotateInDownLeft'),
            'rotateInDownRight' => $this->t('rotateInDownRight'),
            'rotateInUpLeft' => $this->t('rotateInUpLeft'),
            'rotateInUpRight' => $this->t('rotateInUpRight'),
          ],
          $this->t('Rotating Exits')->__toString() => [
            'rotateOut' => $this->t('rotateOut'),
            'rotateOutDownLeft' => $this->t('rotateOutDownLeft'),
            'rotateOutDownRight' => $this->t('rotateOutDownRight'),
            'rotateOutUpLeft' => $this->t('rotateOutUpLeft'),
            'rotateOutUpRight' => $this->t('rotateOutUpRight'),
          ],
          $this->t('Sliding Entrances')->__toString() => [
            'slideInUp' => $this->t('slideInUp'),
            'slideInDown' => $this->t('slideInDown'),
            'slideInLeft' => $this->t('slideInLeft'),
            'slideInRight' => $this->t('slideInRight'),
          ],
          $this->t('Sliding Exits')->__toString() => [
            'slideOutUp' => $this->t('slideOutUp'),
            'slideOutDown' => $this->t('slideOutDown'),
            'slideOutLeft' => $this->t('slideOutLeft'),
            'slideOutRight' => $this->t('slideOutRight'),
          ],
          $this->t('Zoom Entrances')->__toString() => [
            'zoomIn' => $this->t('zoomIn'),
            'zoomInDown' => $this->t('zoomInDown'),
            'zoomInLeft' => $this->t('zoomInLeft'),
            'zoomInRight' => $this->t('zoomInRight'),
            'zoomInUp' => $this->t('zoomInUp'),
          ],
          $this->t('Zoom Exits')->__toString() => [
            'zoomOut' => $this->t('zoomOut'),
            'zoomOutDown' => $this->t('zoomOutDown'),
            'zoomOutLeft' => $this->t('zoomOutLeft'),
            'zoomOutRight' => $this->t('zoomOutRight'),
            'zoomOutUp' => $this->t('zoomOutUp'),
          ],
          $this->t('Specials')->__toString() => [
            'hinge' => $this->t('hinge'),
            'jackInTheBox' => $this->t('jackInTheBox'),
            'rollIn' => $this->t('rollIn'),
            'rollOut' => $this->t('rollOut'),
          ],
        ],
        '#required' => FALSE,
        '#multiple' => FALSE,
        '#empty_option' => $this->t('- None -'),
        '#default_value' => $item->{'style_animation_' . $i . '_animation'} ?? '',
        '#description' => $this->t('Choose the animation to run on the event.' . '<br><a style="margin-left:0" class="button" href="https://daneden.github.io/animate.css/" target="_balnk">' . $this->t('Preview') . '</a>'),
        // @todo We'd need a selector to work on ancestor elements only:
        //   '#states' => [
        //          'visible' => [
        //            'select[name$="[style][animations][style_animation_' . $i . '][style_animation_' . $i . '_events]"]:first' => ['filled' => TRUE],
        //          ],
        //        ],.
        '#wrapper_attributes' => ['class' => 'form-item--style-animation-animation'],
      ];

      $element['style']['animations']['style_animation_' . $i]['style_animation_' . $i . '_delay'] = [
        '#title' => t('Animation start delay'),
        '#type' => 'number',
        '#min' => '0',
        '#step' => '1',
        '#default_value' => $item->{'style_animation_' . $i . '_delay'} ?: 0,
        '#required' => TRUE, // Empty number is not allowed
        '#field_suffix' => 'ms',
        '#description' => $this->t('Delay the animation start. Value in milliseconds (1000ms = 1s).'),
        // @todo We'd need a selector to work on ancestor elements only:
        //   '#states' => [
        //          'visible' => [
        //            'select[name$="[style][animations][style_animation_' . $i . '][style_animation_' . $i . '_events]"]:first' => ['filled' => TRUE],
        //          ],
        //        ],.
        '#wrapper_attributes' => ['class' => 'form-item--style-animation-delay'],
      ];

      $element['style']['animations']['style_animation_' . $i]['style_animation_' . $i . '_transition_duration'] = [
        '#title' => t('Duration'),
        '#type' => 'number',
        '#min' => '0',
        '#step' => '1',
        '#default_value' => $item->{'style_animation_' . $i . '_transition_duration'} ?: 0,
        '#required' => TRUE, // Empty number is not allowed
        '#field_suffix' => 'ms',
        '#description' => $this->t('Duration of the animation transition. Value in milliseconds (1000ms = 1s). 0 = Default duration.'),
        // @todo We'd need a selector to work on ancestor elements only:
        //   '#states' => [
        //          'visible' => [
        //            'select[name$="[style][animations][style_animation_' . $i . '][style_animation_' . $i . '_events]"]:first' => ['filled' => TRUE],
        //          ],
        //        ],.
        '#wrapper_attributes' => ['class' => 'form-item--style-animation-transition-duration'],
      ];
    }

    $element['style']['style_textstyle'] = [
      '#type' => 'details',
      '#title' => $this->t('Text Style'),
      '#group' => 'style',
      '#group_name' => 'style',
    ];
    $element['style']['style_textstyle']['style_textalign'] = [
      '#type' => 'select',
      '#title' => $this->t('Text alignment'),
      '#options' => [
        'text-left' => $this->t('Left aligned'),
        'text-right' => $this->t('Right aligned'),
        'text-center' => $this->t('Centered'),
        'text-justify' => $this->t('Justified'),
        // Eg.: Structured selects
        // $this->t('Columns')->__toString() => array(
        //   'text-columns--2' => t('2 Columns'),
        //   'text-columns--3' => t('3 Columns'),
        //   'text-columns--4' => t('4 Columns'),
        //   'text-columns--5' => t('5 Columns'),
        // ),.
      ],
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#empty_option' => $this->t('Default'),
      '#default_value' => $item->style_textalign ?? '',
      '#description' => $this->t('The text alignment.'),
      '#wrapper_attributes' => ['class' => 'form-item--style-textalign'],
    ];

    $element['style']['style_textstyle']['style_textstyle'] = [
      '#type' => 'select',
      '#title' => $this->t('Text Style'),
      '#options' => [
        'lead' => $this->t('Lead'),
      ],
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#empty_option' => $this->t('Default'),
      '#default_value' => $item->style_textstyle ?? '',
      '#description' => $this->t('The text style, if this element contains text.'),
      '#wrapper_attributes' => ['class' => 'form-item--style-textstyle'],
    ];

    $element['style']['style_textstyle']['style_textcolumns'] = [
      '#type' => 'select',
      '#title' => $this->t('Text Columns'),
      '#options' => [
        'text-columns--2' => t('2 Columns'),
        'text-columns--3' => t('3 Columns'),
        'text-columns--4' => t('4 Columns'),
        'text-columns--5' => t('5 Columns'),
      ],
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#empty_option' => $this->t('Default'),
      '#default_value' => $item->style_textcolumns ?? '',
      '#description' => $this->t('Structure text in columns, if this element contains text.'),
      '#wrapper_attributes' => ['class' => 'form-item--style-textcolumns'],
    ];

    // ===================================
    // Expert settings group
    // ===================================
    $element['style']['expert'] = [
      '#type' => 'details',
      '#title' => $this->t('Expert'),
      '#group' => 'style',
    ];
    $element['style']['expert']['classes_additional'] = [
      '#title' => t('Additional classes'),
      '#type' => 'textfield',
      '#default_value' => $item->classes_additional ?? '',
      '#description' => $this->t('<strong>Experts:</strong> Enter special CSS classes to apply, separated by space.'),
      '#wrapper_attributes' => ['class' => 'form-item--classes-additional'],
      '#maxlength' => 2048,
    ];

    $element['style']['expert']['id_attr'] = [
      '#title' => t('ID'),
      '#type' => 'textfield',
      '#default_value' => $item->id_attr ?? '',
      '#description' => $this->t('<strong>Experts:</strong> Enter an ID attribute.'),
      '#wrapper_attributes' => ['class' => 'form-item--id-attr'],
    ];

    // If cardinality is 1, ensure a label is output for the field by wrapping
    // it in a details element.
    if ($this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality() == 1) {
      $element += [
        '#type' => 'details',
        '#open' => $this->getSetting('open') ? TRUE : FALSE,
      ];
    }

    return $element;
  }

  /**
   *
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Turn the nested array structure into a flat key => value array.
    $values_flat = [];
    if (!empty($values)) {
      foreach ($values as $delta => $field) {
        $it = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($field));
        foreach ($it as $k => $v) {
          $values_flat[$delta][$k] = $v;
        }
      }
    }
    return $values_flat;
  }

}

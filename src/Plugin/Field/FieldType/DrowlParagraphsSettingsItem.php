<?php

namespace Drupal\drowl_paragraphs\Plugin\Field\FieldType;

use Drupal\Component\Utility\Html;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;

/**
 * Field type "drowl_paragraphs_settings".
 *
 * @FieldType(
 *   id = "drowl_paragraphs_settings",
 *   label = @Translation("DROWL Paragraphs settings"),
 *   description = @Translation("DROWL Paragraphs settings field."),
 *   category = "drowl_paragraphs",
 *   default_widget = "drowl_paragraphs_settings_default",
 *   default_formatter = "drowl_paragraphs_settings_default",
 * )
 */
class DrowlParagraphsSettingsItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $output = [];

    $output['columns']['layout_margin_top'] = [
      'type' => 'varchar',
      'length' => 16,
    ];
    $output['columns']['layout_margin_right'] = [
      'type' => 'varchar',
      'length' => 16,
    ];
    $output['columns']['layout_margin_bottom'] = [
      'type' => 'varchar',
      'length' => 16,
    ];
    $output['columns']['layout_margin_left'] = [
      'type' => 'varchar',
      'length' => 16,

    ];
    $output['columns']['layout_padding_top'] = [
      'type' => 'varchar',
      'length' => 16,

    ];
    $output['columns']['layout_padding_right'] = [
      'type' => 'varchar',
      'length' => 16,
    ];
    $output['columns']['layout_padding_bottom'] = [
      'type' => 'varchar',
      'length' => 16,
    ];
    $output['columns']['layout_padding_left'] = [
      'type' => 'varchar',
      'length' => 16,
    ];

    $animations_allowed_count = 4;
    for ($i = 1; $i <= $animations_allowed_count; $i++) {
      $output['columns']['style_animation_' . $i . '_events'] = [
        'type' => 'varchar',
        'length' => 64,
      ];
      $output['columns']['style_animation_' . $i . '_animation'] = [
        'type' => 'varchar',
        'length' => 64,
      ];
      $output['columns']['style_animation_' . $i . '_offset'] = [
        'type' => 'int',
        'size' => 'tiny',
      ];
      $output['columns']['style_animation_' . $i . '_delay'] = [
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
      ];
      $output['columns']['style_animation_' . $i . '_transition_duration'] = [
        'type' => 'int',
        'size' => 'small',
        'unsigned' => TRUE,
      ];
    }

    $output['columns']['layout_min_height'] = [
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
    ];
    $output['columns']['equal_height_group'] = [
      'type' => 'varchar',
      'length' => 255,
    ];
    $output['columns']['style_boxstyle'] = [
      'type' => 'varchar',
      'length' => 64,
    ];
    $output['columns']['style_boxstyle_outline'] = [
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ];
    $output['columns']['style_textalign'] = [
      'type' => 'varchar',
      'length' => 64,
    ];
    $output['columns']['style_textstyle'] = [
      'type' => 'varchar',
      'length' => 64,
    ];
    $output['columns']['style_textcolumns'] = [
      'type' => 'varchar',
      'length' => 64,
    ];
    $output['columns']['classes_additional'] = [
      'type' => 'varchar',
      'length' => 2048,
    ];
    $output['columns']['style_cutline'] = [
      'type' => 'varchar',
      'length' => 64,
    ];
    $output['columns']['id_attr'] = [
      'type' => 'varchar',
      'length' => 128,
    ];
    return $output;

  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];

    $properties['layout_margin_top'] = DataDefinition::create('string')
      ->setLabel(t('Margin top'))
      ->setRequired(FALSE);
    $properties['layout_margin_right'] = DataDefinition::create('string')
      ->setLabel(t('Margin right'))
      ->setRequired(FALSE);
    $properties['layout_margin_bottom'] = DataDefinition::create('string')
      ->setLabel(t('Margin bottom'))
      ->setRequired(FALSE);
    $properties['layout_margin_left'] = DataDefinition::create('string')
      ->setLabel(t('Margin left'))
      ->setRequired(FALSE);

    $properties['layout_padding_top'] = DataDefinition::create('string')
      ->setLabel(t('Padding top'))
      ->setRequired(FALSE);
    $properties['layout_padding_right'] = DataDefinition::create('string')
      ->setLabel(t('Padding right'))
      ->setRequired(FALSE);
    $properties['layout_padding_bottom'] = DataDefinition::create('string')
      ->setLabel(t('Padding bottom'))
      ->setRequired(FALSE);
    $properties['layout_padding_left'] = DataDefinition::create('string')
      ->setLabel(t('Padding left'))
      ->setRequired(FALSE);

    $animations_allowed_count = 4;
    for ($i = 1; $i <= $animations_allowed_count; $i++) {
      $properties['style_animation_' . $i . '_events'] = DataDefinition::create('string')
        ->setLabel(t('Events'))
        ->setRequired(FALSE);
      $properties['style_animation_' . $i . '_animation'] = DataDefinition::create('string')
        ->setLabel(t('Animation'))
        ->setRequired(FALSE);
      $properties['style_animation_' . $i . '_offset'] = DataDefinition::create('string')
        ->setLabel(t('Viewport animation offset trigger'))
        ->setRequired(FALSE);
      $properties['style_animation_' . $i . '_delay'] = DataDefinition::create('integer')
        ->setLabel(t('Animation delay (ms)'))
        ->setRequired(FALSE);
      $properties['style_animation_' . $i . '_transition_duration'] = DataDefinition::create('integer')
        ->setLabel(t('Transition duration (ms)'))
        ->setRequired(FALSE);
    }

    $properties['layout_min_height'] = DataDefinition::create('integer')
      ->setLabel(t('Min height'))
      ->setRequired(FALSE);
    $properties['equal_height_group'] = DataDefinition::create('string')
      ->setLabel(t('Equal height group'))
      ->setRequired(FALSE);
    $properties['style_boxstyle'] = DataDefinition::create('string')
      ->setLabel(t('Box style'))
      ->setRequired(FALSE);
    $properties['style_boxstyle_outline'] = DataDefinition::create('integer')
      ->setLabel(t('Box style outline'))
      ->setRequired(FALSE);
    $properties['style_textalign'] = DataDefinition::create('string')
      ->setLabel(t('Text align'))
      ->setRequired(FALSE);
    $properties['style_textstyle'] = DataDefinition::create('string')
      ->setLabel(t('Text style'))
      ->setRequired(FALSE);
    $properties['style_textcolumns'] = DataDefinition::create('string')
      ->setLabel(t('Text column style'))
      ->setRequired(FALSE);
    $properties['classes_additional'] = DataDefinition::create('string')
      ->setLabel(t('Additional classes'))
      ->setRequired(FALSE);
    $properties['style_cutline'] = DataDefinition::create('string')
      ->setLabel(t('Cutline'))
      ->setRequired(FALSE);
    $properties['id_attr'] = DataDefinition::create('string')
      ->setLabel(t('Custom ID'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $item = $this->getValue();
    if (!empty($item)) {
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key !== '_attributes') {
            // One item is not null.
            if ($value !== NULL && $value !== '') {
              return FALSE;
            }
          }
        }
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    // Sanitize classes_additional:
    $classes_additional = $this->get('classes_additional')->getValue();
    $classes_additional_sanitized = '';
    if (!empty($classes_additional)) {
      $classes_additional_array = explode(' ', trim($classes_additional));
      foreach ($classes_additional_array as $class) {
        $classes_additional_sanitized .= ' ' . trim(Html::getClass(trim($class)));
      }
    }
    $this->get('classes_additional')->setValue(trim($classes_additional_sanitized));

    // Sanitize equal height group:
    $equal_height_group = $this->get('equal_height_group')->getValue();
    if (!empty($equal_height_group)) {
      $this->get('equal_height_group')->setValue(Html::cleanCssIdentifier(trim($equal_height_group)));
    }

    // Sanitize ID:
    $id_attr = $this->get('id_attr')->getValue();
    if (!empty($id_attr)) {
      $this->get('id_attr')->setValue(Html::cleanCssIdentifier(trim($id_attr)));
    }
  }

}

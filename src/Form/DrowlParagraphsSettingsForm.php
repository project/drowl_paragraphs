<?php

namespace Drupal\drowl_paragraphs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;

/**
 * Administration settings form.
 */
class DrowlParagraphsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'drowl_paragraphs_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('drowl_paragraphs.settings');
    $settings = $config->get();

    $slide_amount_options = array_combine(range(1, 10), range(1, 10));

    $form['defaults'] = [
      '#type' => 'details',
      '#attributes' => [
        'id' => 'drowl-paragraphs-defaults-wrapper',
      ],
      '#title' => $this->t('Defaults'),
      '#description' => $this->t('Set the global defaults for DROWL paragraphs.'),
    ];

    $form['defaults']['layout_slideshow'] = [
      '#type' => 'details',
      '#title' => $this->t('Layout Slideshow'),
      '#description' => $this->t('Configuration for paragraph type "Layout Slideshow'),
    ];

    $form['defaults']['layout_slideshow']['layout_section_width'] = [
      '#type' => 'select',
      '#title' => $this->t('Section width'),
      '#options' => [
        'page-width' => $this->t('Page width (all)'),
        'viewport-width' => $this->t('Viewport width (all)'),
        'viewport-width-cp' => $this->t('Viewport width (only background)'),
      ],
      '#default_value' => $settings['defaults']['layout_slideshow']['layout_section_width'] ?? 'viewport-width-cp',
      '#required' => TRUE,
      '#description' => $this->t('Overrides the container width, ignoring the parent container width. Viewport width = screen width, Page width = content width.'),
    ];
    $form['defaults']['layout_slideshow']['autoplay'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autoplay'),
      '#default_value' => $settings['defaults']['layout_slideshow']['autoplay'] ?? TRUE,
      '#description' => $this->t('Plays the slideshow automatically. If this option is deactivated, the user has to forward manually.'),
    ];
    $form['defaults']['layout_slideshow']['auto_height'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic height'),
      '#default_value' => $settings['defaults']['layout_slideshow']['auto_height'] ?? TRUE,
      '#description' => $this->t('Automatically detects each slides height and accordingly changes the slideshow height on scroll.'),
    ];
    $form['defaults']['layout_slideshow']['navigation_arrows'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show arrows navigation'),
      '#default_value' => $settings['defaults']['layout_slideshow']['navigation_arrows'] ?? TRUE,
      '#description' => $this->t('Shows back / forward navigation arrows.'),
    ];
    $form['defaults']['layout_slideshow']['navigation_dots'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show dots navigation'),
      '#default_value' => $settings['defaults']['layout_slideshow']['navigation_dots'] ?? TRUE,
      '#description' => $this->t('Show navigation dots.'),
    ];
    $form['defaults']['layout_slideshow']['infinite'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Infinite loop'),
      '#default_value' => $settings['defaults']['layout_slideshow']['infinite'] ?? TRUE,
      '#description' => $this->t('Displays the slideshow infinitely by restarting at the first element after the last.'),
    ];
    $form['defaults']['layout_slideshow']['center_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Center mode'),
      '#default_value' => $settings['defaults']['layout_slideshow']['center_mode'] ?? TRUE,
      '#description' => $this->t('Allows a centered view by presenting the previous and next slide cropped. For center mode to work, the number of visible slides must be set to an odd number.'),
    ];
    $form['defaults']['layout_slideshow']['controls_outside'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Controls outside'),
      '#default_value' => $settings['defaults']['layout_slideshow']['controls_outside'] ?? TRUE,
      '#description' => $this->t('Shows the controls (arrows / dots) outside the slider so as not to obscure the content.'),
    ];

    $form['defaults']['layout_slideshow']['visible_elements_sm'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Small devices)'),
      '#description' => $this->t('Set the number of slide elements to show on small devices'),
      '#default_value' => $settings['defaults']['layout_slideshow']['visible_elements_sm'] ?? 1,
      '#options' => $slide_amount_options,
    ];
    $form['defaults']['layout_slideshow']['visible_elements_md'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Medium devices)'),
      '#description' => $this->t('Set the number of slide elements to show on medium devices'),
      '#default_value' => $settings['defaults']['layout_slideshow']['visible_elements_md'] ?? 2,
      '#options' => $slide_amount_options,
    ];
    $form['defaults']['layout_slideshow']['visible_elements_lg'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Large devices)'),
      '#description' => $this->t('Set the number of slide elements to show on large devices'),
      '#default_value' => $settings['defaults']['layout_slideshow']['visible_elements_lg'] ?? 3,
      '#options' => $slide_amount_options,
    ];

    $form['defaults']['container_slides'] = [
      '#type' => 'details',
      '#title' => $this->t('Container Slides (DEPRECATED)'),
      '#description' => $this->t('Configuration for paragraph type "Container Slide" (DEPRECATED and replaced by Layout Slideshow)'),
    ];
    $form['defaults']['container_slides']['default_slide_amount_sm'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Small devices)'),
      '#description' => $this->t('Set the number of slide elements to show on small devices'),
      '#default_value' => !empty($settings['defaults']['container_slides']['default_slide_amount_sm']) ? $settings['defaults']['container_slides']['default_slide_amount_sm'] : 1,
      '#options' => $slide_amount_options,
    ];
    $form['defaults']['container_slides']['default_slide_amount_md'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Medium devices)'),
      '#description' => $this->t('Set the number of slide elements to show on medium devices'),
      '#default_value' => !empty($settings['defaults']['container_slides']['default_slide_amount_md']) ? $settings['defaults']['container_slides']['default_slide_amount_md'] : 1,
      '#options' => $slide_amount_options,
    ];
    $form['defaults']['container_slides']['default_slide_amount_lg'] = [
      '#type' => 'select',
      '#title' => $this->t('Visible elements (Large devices)'),
      '#description' => $this->t('Set the number of slide elements to show on large devices'),
      '#default_value' => !empty($settings['defaults']['container_slides']['default_slide_amount_lg']) ? $settings['defaults']['container_slides']['default_slide_amount_lg'] : 1,
      '#options' => $slide_amount_options,
    ];

    $form['defaults']['breakpoint_mapping_sizes_fallback'] = [
      '#type' => 'details',
      '#title' => $this->t('Breakpoint mapping fallbacks'),
      '#description' => $this->t('Fallback values if breakpoint mappings can not be determined.'),
    ];
    $form['defaults']['breakpoint_mapping_sizes_fallback']['md'] = [
      '#type' => 'textfield',
      '#size' => 5,
      '#title' => $this->t('Breakpoint fallback (Medium devices) in px'),
      '#description' => $this->t('The fallback value to use when the breakpoint could not be determined from Drupal breakpoints.yml (Breakpoint mapping)'),
      '#default_value' => !empty($settings['defaults']['breakpoint_mapping_sizes']['breakpoint_mapping_sizes_fallback_md']) ? $settings['defaults']['breakpoint_mapping_sizes']['breakpoint_mapping_sizes_fallback_md'] : 640,
    ];

    $form['defaults']['breakpoint_mapping_sizes_fallback']['lg'] = [
      '#type' => 'textfield',
      '#size' => 5,
      '#title' => $this->t('Breakpoint fallback (Large devices) in px'),
      '#description' => $this->t('The fallback value to use when the breakpoint could not be determined from Drupal breakpoints.yml (Breakpoint mapping)'),
      '#default_value' => !empty($settings['defaults']['breakpoint_mapping_sizes']['breakpoint_mapping_sizes_fallback_lg']) ? $settings['defaults']['breakpoint_mapping_sizes']['breakpoint_mapping_sizes_fallback_lg'] : 1024,
    ];

    // ===================== BREAKPOINTS ===================================
    $breakpointManager = \Drupal::service('breakpoint.manager');
    $breakpoint_group = $form_state->hasValue([
      'breakpoints',
      'breakpoint_group',
    ]) ? $form_state->getValue([
      'breakpoints',
      'breakpoint_group',
    ]) : ($settings['breakpoint_group'] ?? 'seven');

    $form['breakpoints'] = [
      '#type' => 'details',
      '#attributes' => [
        'id' => 'drowl-paragraphs-breakpoints-wrapper',
      ],
      '#title' => $this->t('Breakpoint mapping'),
      '#description' => $this->t('Define the breakpoint mapping for DROWL paragraphs Small / Medium / Large settings, for example for slick.'),
    ];

    $form['breakpoints']['breakpoint_group'] = [
      '#type' => 'select',
      '#title' => $this->t('Breakpoint group'),
      '#default_value' => $breakpoint_group,
      '#options' => $breakpointManager->getGroups(),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::breakpointMappingFormAjax',
        'wrapper' => 'drowl-paragraphs-breakpoint-mapping',
      ],
    ];

    $form['breakpoints']['breakpoint_mapping'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'drowl-paragraphs-breakpoint-mapping',
      ],
    ];

    $breakpoints = $breakpointManager->getBreakpointsByGroup($breakpoint_group);
    foreach ($breakpoints as $breakpoint_id => $breakpoint) {
      foreach ($breakpoint->getMultipliers() as $multiplier) {
        // We have to explode group and id here for the array otherwise config will not save it and throw an exception because dots are not allowed as keys in configuration.
        $breakpoint_id_exploded = explode('.', $breakpoint_id);
        $group = $breakpoint_id_exploded[0];
        $id = $breakpoint_id_exploded[1];
        $label = $multiplier . ' ' . $breakpoint->getLabel() . ' [' . $breakpoint->getMediaQuery() . ']';
        $form['breakpoints']['breakpoint_mapping'][$group][$id][$multiplier] = [
          '#type' => 'details',
          '#title' => $label,
        ];

        $form['breakpoints']['breakpoint_mapping'][$group][$id][$multiplier]['size_mapping'] = [
          '#title' => $this->t('Map device size'),
          '#type' => 'select',
          '#options' => [
            'sm' => $this->t('Small devices'),
            'md' => $this->t('Medium devices'),
            'lg' => $this->t('Large devices'),
          ],
          '#default_value' => $settings['breakpoint_mapping'][$group][$id][$multiplier]['size_mapping'] ?? NULL,
        ];
      }
    }
    // ===================== BREAKPOINTS END ===================================
    // ===================== OPTIONS START ===================================
    $form['options'] = [
      '#type' => 'details',
      '#attributes' => [
        'id' => 'drowl-paragraphs-options-wrapper',
      ],
      '#title' => $this->t('Options'),
      '#description' => $this->t('Configure DROWL Paragraphs form select options.'),
    ];

    $options_style_boxstyle_array_default = [
      'default' => 'Default',
      'primary' => 'Primary',
      'secondary' => 'Secondary',
      'grey-light' => 'Grey Light',
      'grey-dark' => 'Grey Dark',
      'light' => 'Light',
      'dark' => 'Dark',
      'transparent-light' => 'Transparent (100%), text light',
      'transparent-dark' => 'Transparent (100%), text dark',
      'light-glass' => 'Transparent light, text dark',
      'dark-glass' => 'Transparent dark, text light',
      'info' => 'Info',
      'warning' => 'Warning',
      'alert' => 'Alarm',
      'success' => 'Success',
    ];
    $options_style_boxstyle = !empty($settings['options']['options_style_boxstyle']) ? $settings['options']['options_style_boxstyle'] : $options_style_boxstyle_array_default;
    $form['options']['options_style_boxstyle'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Box style classes'),
      '#default_value' => self::arrayToKeyValueString($options_style_boxstyle),
      '#rows' => 10,
      '#description' => $this->t('Define the available box style classes in the format: "box-style-class|Title". Enter the title untranslated in English.<br />You should NOT REMOVE values here which are in use as it may break things.'),
    ];
    // ===================== OPTIONS END ===================================
    $form['#tree'] = TRUE;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValues();

    // Check $form_values['options']['options_style_boxstyle'].
    $options_style_boxstyle = trim($form_values['options']['options_style_boxstyle']);
    if (empty($options_style_boxstyle) || strpos($options_style_boxstyle, '|') === FALSE) {
      $form_state->setError($form['options']['options_style_boxstyle'], t('Box style classes may not be empty and must match the given KEY|VALUE format.'));
    }
    if (count(self::keyValueStringToArray($options_style_boxstyle)) !== count(explode("\n", $options_style_boxstyle))) {
      $form_state->setError($form['options']['options_style_boxstyle'], t('Box style classes must match the given KEY|VALUE format.'));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('drowl_paragraphs.settings');
    $form_values = $form_state->getValues();

    $breakpoint_group = !empty($form_values['breakpoints']['breakpoint_group']) ? $form_values['breakpoints']['breakpoint_group'] : 'seven';
    $breakpoint_mapping = !empty($form_values['breakpoints']['breakpoint_mapping']) ? $form_values['breakpoints']['breakpoint_mapping'] : [];
    // Calculate min / max for breakpoints.
    $breakpoint_mapping_sizes = [];
    if (!empty($breakpoint_mapping)) {
      $breakpoint_mapping_sizes = self::breakpointMappingToDeviceSizes($breakpoint_mapping);
    }

    // Options.
    $options = $form_values['options'];
    // String to array conversion for options. We're saving them as array!
    $options['options_style_boxstyle'] = self::keyValueStringToArray($options['options_style_boxstyle']);

    $config->set('breakpoint_group', $breakpoint_group)
      ->set('breakpoint_mapping', $breakpoint_mapping)
      ->set('breakpoint_mapping_sizes', $breakpoint_mapping_sizes)
      ->set('defaults', $form_values['defaults'])
      ->set('options', $options)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Get the form for mapping breakpoints to image styles.
   */
  public function breakpointMappingFormAjax($form, FormStateInterface $form_state) {
    return $form['breakpoints']['breakpoint_mapping'];
  }

  /**
   *
   */
  protected static function getMinMaxFromMediaQuery($mediaQuery) {
    $re = '/\d*(min-width|max-width):\s*(\d+\s?)(px|em|rem)/';
    preg_match_all($re, $mediaQuery, $matches, PREG_SET_ORDER, 0);
    $result = [];
    if (!empty($matches)) {
      $matches_count = count($matches);
      if ($matches_count <= 2) {
        foreach ($matches as $match) {
          $match_count = count($match);
          if ($match_count == 4) {
            $lastResult = [
              'mediaQuery' => $mediaQuery,
            // min-width / max-width.
              'type' => $match[1],
            // 1200
              'size' => $match[2],
            // Px / em / rem.
              'unit' => $match[3],
            ];
            $result[$match[1]] = $lastResult;
          }
          if ($matches_count == 1) {
            if ($lastResult['type'] == 'max-width') {
              // Add pseudo entry for the min-width (starting from 0)
              $result['min-width'] = [
                'mediaQuery' => $mediaQuery,
              // min-width / max-width.
                'type' => 'min-width',
              // 1200
                'size' => 0,
              // Px / em / rem.
                'unit' => $lastResult['unit'],
              ];
            }
            elseif ($lastResult['type'] == 'min-width') {
              // Add pseudo entry for the max-width to ensure we always deliver both values:
              // This is a bit dirty. Remove if it leads to more problems than it solves.
              $result['max-width'] = [
                'mediaQuery' => $mediaQuery,
              // min-width / max-width.
                'type' => 'max-width',
              // 1200
                'size' => 99999999,
              // Px / em / rem.
                'unit' => $lastResult['unit'],
              ];
            }
          }
          elseif ($matches_count == 0) {
            \Drupal::logger('drowl_paragraphs')
              ->notice('getMinMaxFromMediaQuery: No matches in @mediaQuery. Query not supported. That may lead to unwanted conditions.', ['@mediaQuery' => $mediaQuery]);
          }
        }
      }
    }
    else {
      \Drupal::logger('drowl_paragraphs')
        ->notice('getMinMaxFromMediaQuery: More than 2 matches in @mediaQuery. Query not supported. That may lead to unwanted conditions.', ['@mediaQuery' => $mediaQuery]);
    }
    return $result;
  }

    /**
   * Helper function to calculate the breakpoint mapping sizes from breakpoints.
   *
   * @param array $breakpoint_mapping
   *   The breakpoint mapping array.
   * @return array
   *   The breakpoint_mapping_sizes array.
   */
  public static function breakpointMappingToDeviceSizes(array $breakpoint_mapping = []): array {
    $breakpoint_mapping_sizes = [];
    if (!empty($breakpoint_mapping)) {
      foreach ($breakpoint_mapping as $group => $idArray) {
        foreach ($idArray as $id => $multiplierArray) {
          foreach ($multiplierArray as $multiplier => $size) {
            $sizeMapping = $size['size_mapping'];
            $breakpointManager = \Drupal::service('breakpoint.manager');
            $breakpoints = $breakpointManager->getBreakpointsByGroup($group);
            $breakpoint = $breakpoints[$group . '.' . $id];
            $mediaQuery = $breakpoint->getMediaQuery();
            $minMaxArray = self::getMinMaxFromMediaQuery($mediaQuery);
            if (!empty($minMaxArray)) {
              foreach ($minMaxArray as $type => $values) {
                if (!empty($breakpoint_mapping_sizes[$sizeMapping][$type])) {
                  $compare = $breakpoint_mapping_sizes[$sizeMapping][$type];
                  if ($type == 'max-width' && (int) $values['size'] > (int) $compare['size']) {
                    // Override if compared max-width is higher.
                    $breakpoint_mapping_sizes[$sizeMapping][$type] = $values;
                  }
                  elseif ($type == 'min-width' && (int) $values['size'] < (int) $compare['size']) {
                    // Override if compared max-width is higher.
                    $breakpoint_mapping_sizes[$sizeMapping][$type] = $values;
                  }
                }
                else {
                  // No values for this size yet:
                  $breakpoint_mapping_sizes[$sizeMapping][$type] = $values;
                }
              }
            }
          }
        }
      }
    }
    return $breakpoint_mapping_sizes;
  }

  /**
   * Generates a string representation of an array of 'allowed values'.
   *
   * This string format is suitable for edition in a textarea.
   *
   * @param array $values
   *   An array of values, where array keys are values and array values are
   *   labels.
   *
   * @return string
   *   The string representation of the $values array:
   *    - Values are separated by a carriage return.
   *    - Each value is in the format "value|label" or "value".
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListItemBase::allowedValuesString()
   */
  protected static function arrayToKeyValueString(array $values = []) {
    $lines = [];
    // Flatten the options to remove possible duplicates:
    $values = OptGroup::flattenOptions($values);
    foreach ($values as $key => $value) {
      $lines[] = "$key|$value";
    }
    return implode("\n", $lines);
  }

  /**
   * Extracts the allowed values array from the given string.
   *
   * @see \Drupal\options\Plugin\Field\FieldType\ListItemBase::extractAllowedValues()
   */
  protected static function keyValueStringToArray($string) {
    $values = [];
    $list = explode("\n", $string);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');
    $generated_keys = $explicit_keys = FALSE;
    foreach ($list as $position => $text) {
      $matches = [];
      if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
        // Trim key and value to avoid unwanted spaces issues.
        $key = trim($matches[1]);
        $value = trim($matches[2]);
      }
      $values[$key] = $value;
    }
    return $values;
  }

}

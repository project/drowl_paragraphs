<?php

/**
 * @file
 * Install, update and uninstall functions for the drowl_paragraphs module.
 */

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageException;

/**
 * Implements hook_requirements().
 */
function drowl_paragraphs_requirements($phase) {
  if ($phase != 'runtime') {
    return [];
  }
  $requirements = [];

  $rellaxLibrary = \Drupal::service('library.discovery')->getLibraryByName('drowl_paragraphs', 'parallax');
  $vergeLibrary = \Drupal::service('library.discovery')->getLibraryByName('drowl_paragraphs', 'global');
  $drowlAdminIconsetLibrary = \Drupal::service('library.discovery')->getLibraryByName('drowl_paragraphs', 'admin_iconset');

  if (empty($rellaxLibrary) || empty($vergeLibrary) || empty($drowlAdminIconsetLibrary)) {
    throw new Exception("Required libraries for the drowl_paragraphs module are not defined in the appropriate libraries.yml file.");
  }

  $rellaxLibraryExist = file_exists(DRUPAL_ROOT . '/' . $rellaxLibrary['js'][0]['data']);
  $vergeLibraryExist = file_exists(DRUPAL_ROOT . '/' . $vergeLibrary['js'][0]['data']);
  $drowlAdminIconseExist = file_exists(DRUPAL_ROOT . '/' . $drowlAdminIconsetLibrary['css'][0]['data']);

  $requirements['rellax'] = [
    'title' => t('Drowl Paragraphs: Rellax library (npm-asset/rellax)'),
    'value' => $rellaxLibraryExist ? t('Installed') : t('Not installed'),
    'description' => $rellaxLibraryExist ? '' : t('To use the drowl_paragraphs module, the rellax library needs to be required via composer, using "composer require npm-asset/rellax". NOTE, that you need set up the "asset-packagist" repository in your projects composer.json file.<br>See <a href="https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries">here</a> on how to do it.'),
    'severity' => $rellaxLibraryExist ? REQUIREMENT_OK : REQUIREMENT_ERROR,
  ];
  $requirements['verge'] = [
    'title' => t('Drowl Paragraphs: Verge library (npm-asset/verge)'),
    'value' => $vergeLibraryExist ? t('Installed') : t('Not installed'),
    'description' => $vergeLibraryExist ? '' : t('To use the drowl_paragraphs module, the verge library needs to be required via composer, using "composer require npm-asset/verge". NOTE, that you need to set up the "asset-packagist" repository in your projects composer.json file.<br>See <a href="https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries">here</a> on how to do it.'),
    'severity' => $vergeLibraryExist ? REQUIREMENT_OK : REQUIREMENT_ERROR,
  ];
  $requirements['drowl_admin_iconset'] = [
    'title' => t('Drowl Paragraphs: Drowl Admin Iconset library (npm-asset/drowl-admin-iconset)'),
    'value' => $drowlAdminIconseExist ? t('Installed') : t('Not installed'),
    'description' => $drowlAdminIconseExist ? '' : t('To use the drowl_paragraphs module, the drowl admin iconset library needs to be required via composer, using "composer require npm-asset/drowl-admin-iconset". NOTE, that you need to set up the "asset-packagist" repository in your projects composer.json file.<br>See <a href="https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#third-party-libraries">here</a> on how to do it.'),
    'severity' => $drowlAdminIconseExist ? REQUIREMENT_OK : REQUIREMENT_ERROR,
  ];
  return $requirements;
}

/**
 * Add further columns: style_textalign, style_textcolumns.
 */
function drowl_paragraphs_update_8003() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_add = ['style_textalign', 'style_textcolumns'];
  return _field_type_schema_column_add_helper($field_type, $columns_to_add);
}

/**
 * Set layout_XX_columns UNSIGNED = FALSE.
 */
function drowl_paragraphs_update_8004() {
  $field_type = 'drowl_paragraphs_settings';
  // $columns_changed = ['layout_sm_columns', 'layout_md_columns', 'layout_ld_columns'];
  return _field_type_schema_column_spec_change_helper($field_type);
}

/**
 * Add further columns: style_cutline.
 */
function drowl_paragraphs_update_8005() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_add = ['style_cutline'];
  return _field_type_schema_column_add_helper($field_type, $columns_to_add);
}

/**
 * Add further columns: layout_sm_collapse, layout_md_collapse, layout_lg_collapse.
 */
function drowl_paragraphs_update_8006() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_add = ['layout_sm_collapse', 'layout_md_collapse', 'layout_lg_collapse'];
  return _field_type_schema_column_add_helper($field_type, $columns_to_add);
}

/**
 * Add further columns: id_attr.
 */
function drowl_paragraphs_update_8007() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_add = ['id_attr'];
  return _field_type_schema_column_add_helper($field_type, $columns_to_add);
}

/**
 * Paragraph types template files were moved into the drowl_paragraphs_types submodule. Please delete them in your theme if their content is equally.
 */
function drowl_paragraphs_update_8008() {
  // This hook is just needed for information.
  return t('Paragraph types template files were moved into the drowl_paragraphs_types submodule. Please delete them in your theme if their content is equally.');
}

/**
 * Remove old settings from schema and database: layout_sm_reverse_indent, layout_sm_uncollapse, layout_md_reverse_indent, layout_md_uncollapse, layout_lg_reverse_indent, layout_lg_uncollapse.
 *
 * @return void
 */
function drowl_paragraphs_update_8009() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_remove = [
    'layout_sm_reverse_indent',
    'layout_sm_uncollapse',
    'layout_md_reverse_indent',
    'layout_md_uncollapse',
    'layout_lg_reverse_indent',
    'layout_lg_uncollapse',
  ];
  return _field_type_schema_column_remove_helper($field_type, $columns_to_remove);
}

/**
 * Remove settings which are no more required in 8.x-3.x because they were solved by Layouts.
 *
 * @return void
 */
function drowl_paragraphs_update_8300() {
  // After this migration now remove the deprecated settings:
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_remove = [
    'layout_sm_columns',
    'layout_sm_indent',
    'layout_sm_collapse',
    'layout_md_columns',
    'layout_md_indent',
    'layout_md_collapse',
    'layout_lg_columns',
    'layout_lg_indent',
    'layout_lg_collapse',
    'layout_section_width',
    'layout_align_children_vertical',
    'layout_align_children_horizontal',
    'layout_reverse_order',
  ];
  return _field_type_schema_column_remove_helper($field_type, $columns_to_remove);
}

/**
 * Update entity schema accordingly to settings changes for paragraph_settings field.
 *
 * @param bool $sandbox
 *
 * @return void
 */
function drowl_paragraphs_update_8301(&$sandbox) {
  /*
   * We should now be able to solve
   * updates cleaner:
   * https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Entity%21EntityDefinitionUpdateManager.php/class/EntityDefinitionUpdateManager/8.2.x
   * See:
   * - https://www.agoradesign.at/blog/why-running-drupalentitydefinitionupdatemanager-applyupdates-update-hooks-bad
   * - http://datatrans.demo.md-systems.ch/api/paragraphs/paragraphs%21paragraphs.install/function/paragraphs_update_8011/8.x-1.x
   * - https://drupal.stackexchange.com/questions/230793/how-can-i-update-the-length-of-a-base-field-definition-in-an-update-hook
   * - https://www.drupal.org/node/2554097
   * - https://www.drupal.org/docs/8/api/entity-api/defining-and-using-content-entity-field-definitions
   * - https://www.drupal.org/docs/8/api/entity-api/defining-and-using-content-entity-field-definitions
   */
  // @todo Is this still correct and makes sense??
  $entity_type = \Drupal::service('entity_type.manager')->getDefinition('paragraph');
  \Drupal::service('entity.definition_update_manager')->updateEntityType($entity_type);
}

/**
 * Add further columns: style_boxstyle_outline.
 */
function drowl_paragraphs_update_8302() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_add = ['style_boxstyle_outline'];
  return _field_type_schema_column_add_helper($field_type, $columns_to_add);
}

/**
 * Update style_boxstyle_outline schema.
 */
function drowl_paragraphs_update_8303() {
  $field_type = 'drowl_paragraphs_settings';
  // $columns_changed = ['style_boxstyle_outline'];
  return _field_type_schema_column_spec_change_helper($field_type);
}

/**
 * Drowl_paragraphs_anchor_menu submodule was superseeded by foundation_anchor_menu_block. Remove the old module and install the new one.
 * You will have to add the new block manually. Anchor paragraphs are working in combination with the new module without modification.
 */
function drowl_paragraphs_update_8304(&$sandbox) {
  if (\Drupal::service('module_handler')->moduleExists('drowl_paragraphs_anchor_menu')) {
    // Remove the old module:
    \Drupal::service('module_installer')->uninstall(['drowl_paragraphs_anchor_menu']);

    // Install the new module if it exists and drowl_paragraphs_anchor_menu was installed before:
    if (\Drupal::service('module_handler')->moduleExists('foundation_anchor_menu_block')) {
      \Drupal::service('module_installer')->install(['foundation_anchor_menu_block']);
    }

    // Leave information for manual steps:
    return t('drowl_paragraphs_anchor_menu submodule was superseed by foundation_anchor_menu_block. drowl_paragraphs_anchor_menu has been uninstalled. You will have to add the new anchor menu block manually. Existing anchor paragraphs are working in combination with the new module without modification!');
  }
  return t('You did not use drowl_paragraphs_anchor_menu submodule before, which was now removed. No changes required.');
}

/**
 * Make style_boxstyle options configurable in the settings form.
 * Move the selectable options to the configuration.
 *
 * @param [type] $sandbox
 *
 * @return void
 */
function drowl_paragraphs_update_8305(&$sandbox) {
  $configEditable = \Drupal::service('config.factory')->getEditable('drowl_paragraphs.settings');
  // This were the form options before:
  $options['options_style_boxstyle'] = [
    'default' => 'Default',
    'primary' => 'Primary',
    'secondary' => 'Secondary',
    'grey-light' => 'Grey Light',
    'grey-dark' => 'Grey Dark',
    'light' => 'Light',
    'dark' => 'Dark',
    'transparent-light' => 'Transparent (100%), text light',
    'transparent-dark' => 'Transparent (100%), text dark',
    'light-glass' => 'Transparent light, text dark',
    'dark-glass' => 'Transparent dark, text light',
    'info' => 'Info',
    'warning' => 'Warning',
    'alert' => 'Alarm',
    'success' => 'Success',
  ];
  // The 'options' setting is new, so we can simply set it:
  $configEditable->set('options', $options)->save();
}

/**
 * Add further columns: equal_height_group.
 */
function drowl_paragraphs_update_8306() {
  $field_type = 'drowl_paragraphs_settings';
  $columns_to_add = ['equal_height_group'];
  return _field_type_schema_column_add_helper($field_type, $columns_to_add);
}

/**
 * Increase 'classes_additional' max_length.
 */
function drowl_paragraphs_update_8307() {
  _field_type_schema_column_spec_change_helper('drowl_paragraphs_settings');
}

/**
 * Add the new layout_slideshow default settings.
 */
function drowl_paragraphs_update_8308(&$sandbox) {
  $configEditable = \Drupal::service('config.factory')->getEditable('drowl_paragraphs.settings');
  $defaults = $configEditable->get('defaults');
  // This were the form options before:
  $defaults['layout_slideshow'] = [
    'layout_section_width' => 'viewport-width-cp',
    'autoplay' => TRUE,
    'auto_height' => TRUE,
    'navigation_arrows' => TRUE,
    'navigation_dots' => TRUE,
    'infinite' => TRUE,
    'center_mode' => FALSE,
    'controls_outside' => TRUE,
    'visible_elements_sm' => 1,
    'visible_elements_md' => 2,
    'visible_elements_lg' => 3,
  ];
  // The 'options' setting is new, so we can simply set it:
  $configEditable->set('defaults', $defaults)->save();
}

/**
 * Set the correct defaults for breakpoint_mapping.
 */
function drowl_paragraphs_update_8309(&$sandbox) {
  $configEditable = \Drupal::service('config.factory')->getEditable('drowl_paragraphs.settings');
  $breakpoint_mapping = $configEditable->get('breakpoint_mapping');
  // Add / override the settings for drowl_base:
  $breakpoint_mapping['drowl_base'] = [
    'small' => [
      '1x' => [
        'size_mapping' => 'sm',
      ],
    ],
    'medium' => [
      '1x' => [
        'size_mapping' => 'md',
      ],
    ],
    'large' => [
      '1x' => [
        'size_mapping' => 'lg',
      ],
    ],
    'xlarge' => [
      '1x' => [
        'size_mapping' => 'lg',
      ],
    ],
    'xxlarge' => [
      '1x' => [
        'size_mapping' => 'lg',
      ],
    ],
  ];
  // The 'options' setting is new, so we can simply set it:
  $configEditable->set('breakpoint_mapping', $breakpoint_mapping)->save();
}

/**
 * Recalculate breakpoint_mapping_sizes values.
 */
function drowl_paragraphs_update_8310(&$sandbox) {
  $configEditable = \Drupal::service('config.factory')->getEditable('drowl_paragraphs.settings');
  $breakpoint_mapping = $configEditable->get('breakpoint_mapping');
  $breakpoint_mapping_sizes = \Drupal\drowl_paragraphs\Form\DrowlParagraphsSettingsForm::breakpointMappingToDeviceSizes($breakpoint_mapping);
  $configEditable->set('breakpoint_mapping_sizes', $breakpoint_mapping_sizes)->save();

  return 'Updated breakpoint mapping sizes';
}

/**
 * Append [DEPRECATED] to paragraph type "Container: Slides" label.
 */
function drowl_paragraphs_update_8311(&$sandbox) {
  $configEntity = \Drupal::entityTypeManager()
    ->getStorage('paragraphs_type')
    ->load('container_slides');
  if (!$configEntity) {
    return NULL;
  }
  $configEntity->set('label', 'Container: Slides [DEPRECATED]');
  $configEntity->save();
}

/**
 * Rename paragraph bundle "Layout" to "Layout: Grid".
 */
function drowl_paragraphs_update_8312(&$sandbox) {
  $configEntity = \Drupal::entityTypeManager()
    ->getStorage('paragraphs_type')
    ->load('layout');
  if (!$configEntity) {
    return NULL;
  }
  $configEntity->set('label', 'Layout: Grid');
  $configEntity->save();
}

/**
 * Helper function for HOOK_Update to add columns to the field schema.
 *
 * @param $field_type
 *   The field type id e.g. "drowl_paragraphs_settings"
 * @param array $columns_to_add
 *   array of the column names from schema() function, e.g. ["style_textalign"].
 */
function _field_type_schema_column_add_helper($field_type, array $columns_to_add = []) {
  $processed_fields = [];
  $field_type_manager = \Drupal::service('plugin.manager.field.field_type');
  $field_definition = $field_type_manager->getDefinition($field_type);
  $field_item_class = $field_definition['class'];

  $schema = \Drupal::database()->schema();
  $entity_type_manager = \Drupal::entityTypeManager();
  $entity_field_manager = \Drupal::service('entity_field.manager');
  $entity_field_map = $entity_field_manager->getFieldMapByFieldType($field_type);
  // The key-value collection for tracking installed storage schema.
  $entity_storage_schema_sql = \Drupal::keyValue('entity.storage_schema.sql');
  $entity_definitions_installed = \Drupal::keyValue('entity.definitions.installed');

  foreach ($entity_field_map as $entity_type_id => $field_map) {
    /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $entity_storage */
    $entity_storage = $entity_type_manager->getStorage($entity_type_id);

    // Only SQL storage based entities are supported / throw known exception.
    if (!($entity_storage instanceof SqlContentEntityStorage)) {
      throw new Exception('Only SQL storage based entities are supported. Class "' . get_class($entity_storage) . '" found.');
    }

    $entity_type = $entity_type_manager->getDefinition($entity_type_id);
    $field_storage_definitions = $entity_field_manager->getFieldStorageDefinitions($entity_type_id);
    /** @var Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
    $table_mapping = $entity_storage->getTableMapping($field_storage_definitions);
    // Only need field storage definitions of address fields.
    /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field_storage_definition */
    foreach (array_intersect_key($field_storage_definitions, $field_map) as $field_storage_definition) {
      $field_name = $field_storage_definition->getName();
      try {
        $table = $table_mapping->getFieldTableName($field_name);
      }
      catch (SqlContentEntityStorageException $e) {
        // Custom storage? Broken site? No matter what, if there is no table
        // or column, there's little we can do.
        continue;
      }
      // See if the field has a revision table.
      $revision_table = NULL;
      if ($entity_type->isRevisionable() && $field_storage_definition->isRevisionable()) {
        if ($table_mapping->requiresDedicatedTableStorage($field_storage_definition)) {
          $revision_table = $table_mapping->getDedicatedRevisionTableName($field_storage_definition);
        }
        elseif ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
          $revision_table = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();
        }
      }
      // Load the installed field schema so that it can be updated.
      $schema_key = "$entity_type_id.field_schema_data.$field_name";
      $field_schema_data = $entity_storage_schema_sql->get($schema_key);

      $processed_fields[] = [$entity_type_id, $field_name];
      // Loop over each new column and add it as a schema column change.
      foreach ($columns_to_add as $column_id) {
        $column = $table_mapping->getFieldColumnName($field_storage_definition, $column_id);
        // Add `initial_from_field` to the new spec, as this will copy over
        // the entire data.
        $field_schema = $field_item_class::schema($field_storage_definition);
        $spec = $field_schema['columns'][$column_id];

        // Add the new column.
        $schema->addField($table, $column, $spec);
        if ($revision_table) {
          $schema->addField($revision_table, $column, $spec);
        }

        // Add the new column to the installed field schema.
        if (!empty($field_schema_data)) {
          $field_schema_data[$table]['fields'][$column] = $field_schema['columns'][$column_id];
          $field_schema_data[$table]['fields'][$column]['not null'] = FALSE;
          if ($revision_table) {
            $field_schema_data[$revision_table]['fields'][$column] = $field_schema['columns'][$column_id];
            $field_schema_data[$revision_table]['fields'][$column]['not null'] = FALSE;
          }
        }
      }

      // Save changes to the installed field schema.
      if (!empty($field_schema_data)) {
        $entity_storage_schema_sql->set($schema_key, $field_schema_data);
      }
      if ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
        $key = "$entity_type_id.field_storage_definitions";
        if ($definitions = $entity_definitions_installed->get($key)) {
          $definitions[$field_name] = $field_storage_definition;
          $entity_definitions_installed->set($key, $definitions);
        }
      }
    }
  }
}

/**
 * Helper function for HOOK_Update to update the field schema to the latest definition.
 *
 * Preserves the existing data. Changes have to be compatible to existing data!
 *
 * @param $field_type
 *   The field type id e.g. "drowl_paragraphs_settings"
 */
function _field_type_schema_column_spec_change_helper($field_type) {
  $processed_fields = [];
  $field_type_manager = \Drupal::service('plugin.manager.field.field_type');
  $field_definition = $field_type_manager->getDefinition($field_type);
  $field_item_class = $field_definition['class'];

  $schema = \Drupal::database()->schema();
  $entity_type_manager = \Drupal::entityTypeManager();
  $entity_field_manager = \Drupal::service('entity_field.manager');
  $entity_field_map = $entity_field_manager->getFieldMapByFieldType($field_type);
  // The key-value collection for tracking installed storage schema.
  $entity_storage_schema_sql = \Drupal::keyValue('entity.storage_schema.sql');
  $entity_definitions_installed = \Drupal::keyValue('entity.definitions.installed');

  foreach ($entity_field_map as $entity_type_id => $field_map) {
    /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $entity_storage */
    $entity_storage = $entity_type_manager->getStorage($entity_type_id);
    $entity_type = $entity_type_manager->getDefinition($entity_type_id);
    $field_storage_definitions = $entity_field_manager->getFieldStorageDefinitions($entity_type_id);
    /** @var Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
    $table_mapping = $entity_storage->getTableMapping($field_storage_definitions);
    // Only need field storage definitions of address fields.
    /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field_storage_definition */
    foreach (array_intersect_key($field_storage_definitions, $field_map) as $field_storage_definition) {
      $field_name = $field_storage_definition->getName();
      $tables = [];
      try {
        $table = $table_mapping->getFieldTableName($field_name);
        $tables[] = $table;
      }
      catch (SqlContentEntityStorageException $e) {
        // Custom storage? Broken site? No matter what, if there is no table, there's little we can do.
        continue;
      }
      // See if the field has a revision table.
      $revision_table = NULL;
      if ($entity_type->isRevisionable() && $field_storage_definition->isRevisionable()) {
        if ($table_mapping->requiresDedicatedTableStorage($field_storage_definition)) {
          $revision_table = $table_mapping->getDedicatedRevisionTableName($field_storage_definition);
          $tables[] = $revision_table;
        }
        elseif ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
          $revision_table = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();
          $tables[] = $revision_table;
        }
      }

      $database = \Drupal::database();
      $existing_data = [];
      foreach ($tables as $table) {
        // Get the old data.
        $existing_data[$table] = $database->select($table)
          ->fields($table)
          ->execute()
          ->fetchAll(PDO::FETCH_ASSOC);

        // Wipe it.
        $database->truncate($table)->execute();
      }

      $manager = \Drupal::entityDefinitionUpdateManager();
      $manager->updateFieldStorageDefinition($manager->getFieldStorageDefinition($field_name, $entity_type_id));

      // Restore the data.
      if (!empty($tables)) {
        foreach ($tables as $table) {
          if (!empty($existing_data[$table])) {
            $insert_query = $database
              ->insert($table)
              ->fields(array_keys(end($existing_data[$table])));
            foreach ($existing_data[$table] as $row) {
              $insert_query->values(array_values($row));
            }
            $insert_query->execute();
          }
        }
      }
    }
  }
}

/**
 * Helper function for HOOK_Update to remove columns from the field schema.
 *
 * @param $field_type
 *   The field type id e.g. "drowl_paragraphs_settings"
 * @param array $columns_to_remove
 *   array of the column names from schema() function, e.g. ["style_textalign"].
 */
function _field_type_schema_column_remove_helper($field_type, array $columns_to_remove = []) {
  $processed_fields = [];
  $field_type_manager = \Drupal::service('plugin.manager.field.field_type');
  $field_definition = $field_type_manager->getDefinition($field_type);
  $field_item_class = $field_definition['class'];

  $schema = \Drupal::database()->schema();
  $entity_type_manager = \Drupal::entityTypeManager();
  $entity_field_manager = \Drupal::service('entity_field.manager');
  $entity_field_map = $entity_field_manager->getFieldMapByFieldType($field_type);
  // The key-value collection for tracking installed storage schema.
  $entity_storage_schema_sql = \Drupal::keyValue('entity.storage_schema.sql');
  $entity_definitions_installed = \Drupal::keyValue('entity.definitions.installed');

  foreach ($entity_field_map as $entity_type_id => $field_map) {
    /** @var \Drupal\Core\Entity\Sql\SqlContentEntityStorage $entity_storage */
    $entity_storage = $entity_type_manager->getStorage($entity_type_id);

    // Only SQL storage based entities are supported / throw known exception.
    if (!($entity_storage instanceof SqlContentEntityStorage)) {
      throw new Exception('Only SQL storage based entities are supported. Class "' . get_class($entity_storage) . '" found.');
    }

    $entity_type = $entity_type_manager->getDefinition($entity_type_id);
    $field_storage_definitions = $entity_field_manager->getFieldStorageDefinitions($entity_type_id);
    /** @var Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
    $table_mapping = $entity_storage->getTableMapping($field_storage_definitions);
    // Only need field storage definitions of address fields.
    /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field_storage_definition */
    foreach (array_intersect_key($field_storage_definitions, $field_map) as $field_storage_definition) {
      $field_name = $field_storage_definition->getName();
      try {
        $table = $table_mapping->getFieldTableName($field_name);
      }
      catch (SqlContentEntityStorageException $e) {
        // Custom storage? Broken site? No matter what, if there is no table
        // or column, there's little we can do.
        continue;
      }
      // See if the field has a revision table.
      $revision_table = NULL;
      if ($entity_type->isRevisionable() && $field_storage_definition->isRevisionable()) {
        if ($table_mapping->requiresDedicatedTableStorage($field_storage_definition)) {
          $revision_table = $table_mapping->getDedicatedRevisionTableName($field_storage_definition);
        }
        elseif ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
          $revision_table = $entity_type->getRevisionDataTable() ?: $entity_type->getRevisionTable();
        }
      }
      // Load the installed field schema so that it can be updated.
      $schema_key = "$entity_type_id.field_schema_data.$field_name";
      $field_schema_data = $entity_storage_schema_sql->get($schema_key);

      $processed_fields[] = [$entity_type_id, $field_name];
      // Loop over each new column and add it as a schema column change.
      foreach ($columns_to_remove as $column_id) {
        $column = $table_mapping->getFieldColumnName($field_storage_definition, $column_id);
        // Add `initial_from_field` to the new spec, as this will copy over
        // the entire data.
        $field_schema = $field_item_class::schema($field_storage_definition);
        $spec = $field_schema['columns'][$column_id];

        // Add the new column.
        $schema->dropField($table, $column);
        if ($revision_table) {
          $schema->dropField($revision_table, $column);
        }

        // Remove the column from the installed field schema.
        if (!empty($field_schema_data)) {
          unset($field_schema_data[$table]['fields'][$column]);
          $field_schema_data[$table]['fields'][$column]['not null'] = FALSE;
          if ($revision_table) {
            unset($field_schema_data[$revision_table]['fields'][$column]);
          }
        }
      }

      // Save changes to the installed field schema.
      if (!empty($field_schema_data)) {
        $entity_storage_schema_sql->set($schema_key, $field_schema_data);
      }
      if ($table_mapping->allowsSharedTableStorage($field_storage_definition)) {
        $key = "$entity_type_id.field_storage_definitions";
        if ($definitions = $entity_definitions_installed->get($key)) {
          $definitions[$field_name] = $field_storage_definition;
          $entity_definitions_installed->set($key, $definitions);
        }
      }
    }
  }
}
